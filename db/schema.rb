# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161110140810) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "business_units", force: :cascade do |t|
    t.string   "title",                          null: false
    t.boolean  "is_headquater",  default: false
    t.string   "address_line_1",                 null: false
    t.string   "address_line_2"
    t.string   "city",                           null: false
    t.string   "zipcode"
    t.string   "state"
    t.string   "country",                        null: false
    t.string   "phone",                          null: false
    t.string   "fax"
    t.string   "url"
    t.integer  "company_id",                     null: false
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
  end

  create_table "chat_messages", force: :cascade do |t|
    t.text     "body"
    t.integer  "employee_id"
    t.integer  "chat_room_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["chat_room_id"], name: "index_chat_messages_on_chat_room_id", using: :btree
    t.index ["employee_id"], name: "index_chat_messages_on_employee_id", using: :btree
  end

  create_table "chat_room_members", force: :cascade do |t|
    t.integer  "employee_id"
    t.integer  "chat_room_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["chat_room_id"], name: "index_chat_room_members_on_chat_room_id", using: :btree
    t.index ["employee_id"], name: "index_chat_room_members_on_employee_id", using: :btree
  end

  create_table "chat_rooms", force: :cascade do |t|
    t.string   "title"
    t.integer  "employee_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["employee_id"], name: "index_chat_rooms_on_employee_id", using: :btree
  end

  create_table "companies", force: :cascade do |t|
    t.string   "name",              null: false
    t.text     "description"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.string   "logo_file_name"
    t.string   "logo_content_type"
    t.integer  "logo_file_size"
    t.datetime "logo_updated_at"
  end

  create_table "departments", force: :cascade do |t|
    t.string   "title",            null: false
    t.string   "description"
    t.integer  "business_unit_id", null: false
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  create_table "dimensions", force: :cascade do |t|
    t.string   "title",          null: false
    t.text     "description"
    t.integer  "created_by"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.integer  "reference_id"
    t.string   "reference_type"
  end

  create_table "employee_departments", force: :cascade do |t|
    t.integer  "employee_id",                        null: false
    t.integer  "department_id",                      null: false
    t.boolean  "is_department_head", default: false
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
  end

  create_table "employee_interests", force: :cascade do |t|
    t.integer  "employee_id", null: false
    t.integer  "interest_id", null: false
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "employee_skills", force: :cascade do |t|
    t.integer  "employee_id", null: false
    t.integer  "skill_id",    null: false
    t.boolean  "is_primary"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "employee_teams", force: :cascade do |t|
    t.integer  "employee_id", null: false
    t.integer  "team_id",     null: false
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "employees", force: :cascade do |t|
    t.string   "first_name",                        null: false
    t.string   "middle_name"
    t.string   "last_name"
    t.string   "gender",                            null: false
    t.date     "date_of_birth",                     null: false
    t.string   "marital_status"
    t.string   "employee_code",                     null: false
    t.date     "joining_date",                      null: false
    t.string   "mobile"
    t.string   "office_phone"
    t.string   "office_phone_ext"
    t.string   "address_line_1",                    null: false
    t.string   "address_line_2"
    t.string   "city",                              null: false
    t.string   "state"
    t.string   "zip"
    t.string   "country",                           null: false
    t.text     "about"
    t.string   "linked_url"
    t.string   "github_url"
    t.string   "facebook_url"
    t.string   "blog_url"
    t.string   "other_references"
    t.integer  "job_title_id"
    t.integer  "reports_to"
    t.integer  "created_by"
    t.integer  "updated_by"
    t.boolean  "is_active",          default: true
    t.integer  "business_unit_id"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
    t.json     "linkedin_info"
  end

  create_table "entity_dimentions", force: :cascade do |t|
    t.string   "entity_type"
    t.integer  "entity_id"
    t.integer  "dimension_id"
    t.integer  "created_by"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["entity_type", "entity_id"], name: "index_entity_dimentions_on_entity_type_and_entity_id", using: :btree
  end

  create_table "goals", force: :cascade do |t|
    t.string   "title",          null: false
    t.string   "description"
    t.integer  "created_by"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.integer  "reference_id"
    t.string   "reference_type"
  end

  create_table "interests", force: :cascade do |t|
    t.string   "title",      null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "job_titles", force: :cascade do |t|
    t.string   "title"
    t.string   "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "message_categories", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "messages", force: :cascade do |t|
    t.text     "body"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.boolean  "anonymous",           default: false
    t.string   "status",              default: "Unread"
    t.integer  "message_category_id"
    t.integer  "sender_id"
    t.integer  "recipient_id"
    t.datetime "read_at"
    t.index ["message_category_id"], name: "index_messages_on_message_category_id", using: :btree
    t.index ["recipient_id"], name: "index_messages_on_recipient_id", using: :btree
    t.index ["sender_id"], name: "index_messages_on_sender_id", using: :btree
  end

  create_table "posts", force: :cascade do |t|
    t.string   "name"
    t.string   "title"
    t.text     "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "user_id",    null: false
  end

  create_table "roles", force: :cascade do |t|
    t.string   "name",        null: false
    t.string   "title",       null: false
    t.text     "description", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "skills", force: :cascade do |t|
    t.string   "title",      null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "teams", force: :cascade do |t|
    t.string   "title",         null: false
    t.string   "description"
    t.integer  "department_id", null: false
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                             default: "", null: false
    t.string   "encrypted_password",                default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                     default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                                     null: false
    t.datetime "updated_at",                                     null: false
    t.integer  "role_id"
    t.string   "authentication_token",   limit: 30
    t.integer  "employee_id",                                    null: false
    t.index ["authentication_token"], name: "index_users_on_authentication_token", unique: true, using: :btree
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

  add_foreign_key "chat_messages", "chat_rooms"
  add_foreign_key "chat_messages", "employees"
  add_foreign_key "chat_room_members", "chat_rooms"
  add_foreign_key "chat_room_members", "employees"
  add_foreign_key "chat_rooms", "employees"
end
