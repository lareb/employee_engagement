class CreateEmployeeSkills < ActiveRecord::Migration[5.0]
  def change
    create_table :employee_skills do |t|
    	t.integer :employee_id, null: false
    	t.integer :skill_id, null: false
      	t.boolean :is_primary
      	t.timestamps
    end
  end
end
