class CreateDimensions < ActiveRecord::Migration[5.0]
  def change
    create_table :dimensions do |t|
    	t.string :title, null: false
    	t.string :description
    	t.integer :created_by
      	t.timestamps
    end
  end
end
