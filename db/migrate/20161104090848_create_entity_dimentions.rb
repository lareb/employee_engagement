class CreateEntityDimentions < ActiveRecord::Migration[5.0]
  def change
    create_table :entity_dimentions do |t|
    	t.references :entity, polymorphic: true
		t.integer :dimension_id
		t.integer :created_by
      	t.timestamps
    end
  end
end
