class CreateEmployees < ActiveRecord::Migration[5.0]
  def change
    create_table :employees do |t|

      t.string :first_name, null: false
      t.string :middle_name
      t.string :last_name
      t.string :gender, null: false
      t.date :date_of_birth, null: false
      t.string :marital_status
      t.string :employee_code, null: false
      t.date :joining_date, null: false
      t.string :mobile
      t.string :office_phone
      t.string :office_phone_ext
      t.string :address_line_1, null: false
      t.string :address_line_2
      t.string :city, null: false
      t.string :state
      t.string :zip
      t.string :country, null: false
      t.text :about

      #professional networking url
      t.string :linked_url
      t.string :github_url
      t.string :facebook_url
      t.string :blog_url
      t.string :other_references

      t.integer :job_title_id
      t.integer :reporting_manager_id #inner join
      t.integer :created_by
      t.integer :updated_by
      t.boolean :is_active, default: true

      #employee's relation with Business unit, department and team
      t.integer :user_id
      # t.integer :team_id, null: false
      # t.integer :department_id
      t.integer :business_unit_id

      t.timestamps
    end
  end
end
