class ChangeColumnToEmployees < ActiveRecord::Migration[5.0]
  def change
  	rename_column :employees, :reporting_manager_id , :reports_to
  end
end
