class CreateDepartments < ActiveRecord::Migration[5.0]
  def change
    create_table :departments do |t|
    	t.string :title, null: false
    	t.string :description
    	t.integer :business_unit_id, null: false
      t.timestamps
    end
  end
end
