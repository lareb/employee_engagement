class CreateBusinessUnits < ActiveRecord::Migration[5.0]
  def change
    create_table :business_units do |t|
    	t.string :title, null: false
    	t.boolean :is_headquater, default: false
    	t.string :address_line_1, null: false
    	t.string :address_line_2
    	t.string :city, null: false
    	t.string :zipcode
      t.string :state
    	t.string :country, null: false
      t.string :phone, null: false
      t.string :fax
      t.string :url
    	t.integer :company_id, null: false
      t.timestamps
    end
  end
end
