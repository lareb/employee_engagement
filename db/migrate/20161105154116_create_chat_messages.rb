class CreateChatMessages < ActiveRecord::Migration[5.0]
  def change
    create_table :chat_messages do |t|
      t.text :body
      t.references :employee, foreign_key: true
      t.references :chat_room, foreign_key: true

      t.timestamps
    end
  end
end
