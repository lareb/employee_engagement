class CreateEmployeeTeams < ActiveRecord::Migration[5.0]
  def change
    create_table :employee_teams do |t|
    	t.integer :employee_id, null: false
    	t.integer :team_id, null: false
      	t.timestamps
    end
  end
end
