class AddOwnerIdInPostAndMessage < ActiveRecord::Migration[5.0]
  def change
  	add_column(:posts, :user_id, :integer, null: false)
  	add_column(:messages, :user_id, :integer, null: false)
  	remove_column(:messages, :from)  	
  end
end
