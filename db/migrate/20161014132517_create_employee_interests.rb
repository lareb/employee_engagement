class CreateEmployeeInterests < ActiveRecord::Migration[5.0]
  def change
    create_table :employee_interests do |t|
    	t.integer :employee_id, null: false
    	t.integer :interest_id, null: false
		t.timestamps
    end
  end
end
