class AddReferenceIdTypeToDimension < ActiveRecord::Migration[5.0]
  def change
    add_column :dimensions, :reference_id, :integer
    add_column :dimensions, :reference_type, :string
  end
end
