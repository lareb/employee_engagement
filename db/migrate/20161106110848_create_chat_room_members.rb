class CreateChatRoomMembers < ActiveRecord::Migration[5.0]
  def change
    create_table :chat_room_members do |t|
      	t.references :employee, foreign_key: true
      	t.references :chat_room, foreign_key: true
		t.timestamps
    end
  end
end
