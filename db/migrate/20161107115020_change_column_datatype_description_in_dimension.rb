class ChangeColumnDatatypeDescriptionInDimension < ActiveRecord::Migration[5.0]
  def change
  	change_column(:dimensions, :description, :text)
  end
end
