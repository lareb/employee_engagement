class AddNewAndDropOldColumnToMessage < ActiveRecord::Migration[5.0]
  def change
  	remove_column :messages, :subject
  	remove_column :messages, :to
  	remove_column :messages, :user_id
  	rename_column :messages, :content, :body
  	add_column :messages, :anonymous, :boolean, default: false
  	add_column :messages, :status, :string, default: 'Unread'
  	add_reference :messages, :message_category, index: true
  	add_reference :messages, :sender, index: true
  	add_reference :messages, :recipient, index: true
  	add_column :messages, :read_at, :datetime
  end
end
