class RemoveDimensionIdFromGoals < ActiveRecord::Migration[5.0]
  def change
  	remove_column :goals, :dimension_id
  end
end
