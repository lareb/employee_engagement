class CreateTeams < ActiveRecord::Migration[5.0]
  def change
    create_table :teams do |t|
    	t.string :title, null: false
    	t.string :description
    	t.integer :department_id, null: false
      	t.timestamps
    end
  end
end
