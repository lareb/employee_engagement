class AddReferenceIdTypeToGoals < ActiveRecord::Migration[5.0]
  def change
  	add_column :goals, :reference_id, :integer
    add_column :goals, :reference_type, :string
  end
end
