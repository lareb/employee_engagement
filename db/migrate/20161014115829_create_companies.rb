class CreateCompanies < ActiveRecord::Migration[5.0]
	#I don't think so we required this table as we have only one company end of the day
  def change
    create_table :companies do |t|
    	t.string :name, null: false
    	t.text :description
      	t.timestamps
    end
  end
end
