class AddMissingFields < ActiveRecord::Migration[5.0]
  def up
  	add_column(:users, :employee_id, :integer, null: false)
  	remove_column(:employees, :user_id)
  end

  def down
  	remove_column(:users, :employee_id)
  	add_column(:employees, :user_id, :integer)
  end

end
