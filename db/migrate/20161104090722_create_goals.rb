class CreateGoals < ActiveRecord::Migration[5.0]
  def change
    create_table :goals do |t|
    	t.string :title, null: false
    	t.string :description
    	t.integer :dimension_id, null: false
    	t.integer :created_by
      	t.timestamps
    end
  end
end
