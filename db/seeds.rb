

roles = [{name: 'admin', title: 'Administrator', description: 'Administrator'}, {name: 'employee', title: 'Employee of the company', description: 'Employee of the company'}]
Role.create(roles)
e = Employee.create(first_name: "Administrator", date_of_birth: Date.today - 30.years, employee_code: "000", joining_date: Date.today - 10.years, mobile: "9876543210", gender: "Male", address_line_1: "Address line 1", city: "Pune" ,country: "India", photo: File.open(Dir["#{Rails.root}/db/images/male/*.jpg"].sample))
User.create(email: 'admin@forgeahead.io', password: 'password', password_confirmation: 'password', role_id: Role.find_by_name(:admin).try(:id), employee_id: e.id)

puts "-Creating job titles--------"
jt_ceo = JobTitle.create(title: 'Chief Executive Officer')
jt_dep_hd_sale = JobTitle.create(title: 'Vice President')
jt_dep_hd_opr = JobTitle.create(title: 'Chief Operational Officer')
jt_dep_hd_fin = JobTitle.create(title: 'Chief Finance Officer')

jt_dep_hd_si = JobTitle.create(title: 'Network Administrator')
# JobTitle.create(title: 'Network Engineer')
# JobTitle.create(title: 'VP Finance')
jt_dep_hd_hr = JobTitle.create(title: 'HR Administrator')
jt_dep_hd_admin = JobTitle.create(title: 'Sr. Executive - Admin')
jt5 = JobTitle.create(title: 'HR')
jt_dep_hd_eng = JobTitle.create(title: 'Delivery Manager')
jt4 = JobTitle.create(title: 'Project Manager')
JobTitle.create(title: 'Senior Analyst')
JobTitle.create(title: 'Analyst')
JobTitle.create(title: 'Architect')
jt1 = JobTitle.create(title: 'Tech Lead')
jt2 = JobTitle.create(title: 'Senior Software Engineer')
jt3 = JobTitle.create(title: 'Software Engineer')


e_ceo = Employee.create(first_name: "Ashish", last_name: "Shah", date_of_birth: Date.today - 50.years, 
  employee_code: "001", joining_date: Date.today - 16.years, mobile: "9876543211", gender: "Male", 
  address_line_1: "Address line 1", city: "Pune" ,country: "India", photo: File.open("#{Rails.root}/db/images/male/ashish.jpg"))
User.create(email: 'ashish.shah@forgeahead.io', password: 'password', password_confirmation: 'password', role_id: Role.find_by_name(:admin).try(:id), employee_id: e.id)
e_ceo.job_title = jt_ceo
e_ceo.save

e_coo = Employee.create(first_name: "Ashwin", last_name: "Megha", date_of_birth: Date.today - 50.years, 
  employee_code: "002", joining_date: Date.today - 16.years, mobile: "9876543212", gender: "Male", 
  address_line_1: "Address line 1", city: "Pune" ,country: "India", photo: File.open("#{Rails.root}/db/images/male/ashwin.jpg"))
User.create(email: 'ashwin.megha@forgeahead.io', password: 'password', password_confirmation: 'password', role_id: Role.find_by_name(:admin).try(:id), employee_id: e.id)
e_ceo.job_title = jt_dep_hd_opr
e_ceo.save

e_cfo = Employee.create(first_name: "Devesh", last_name: "Hingorani", date_of_birth: Date.today - 45.years, 
  employee_code: "003", joining_date: Date.today - 16.years, mobile: "9876543213", gender: "Male", 
  address_line_1: "Address line 1", city: "Pune" ,country: "India", photo: File.open("#{Rails.root}/db/images/male/devesh.jpg"))
User.create(email: 'devesh.hingorani@forgeahead.io', password: 'password', password_confirmation: 'password', role_id: Role.find_by_name(:admin).try(:id), employee_id: e.id)
e_cfo.job_title = jt_dep_hd_fin
e_cfo.save

puts "Creating company"
logo =  File.open("#{Rails.root}/db/images/fa_logo.png")
c = Company.create({name: 'Forgeahead Solutions', description: 'Forgeahead Solutions', logo: logo})

puts "Creating Dimensions"
d1 = Dimension.create({title: "Culture", 
                       description: "Forgeahead strives to provide to employees a positive, rewarding work environment – an environment that will attract, motivate and retain the best and brightest people. \n Forgeahead provides equal opportunities for growth and development, encouragement to succeed, reviews based on performance and a competitive compensation and benefits package. In return, Forgeahead employees are expected to be individually accountable, to contribute to the team effort, to perform to the best of their abilities and to help make Forgeahead a great place to work.",
                       reference_id: c.id,
                       reference_type: 'Company'})
d2 = Dimension.create({title: "Vision", 
                       description: "Not available",
                       reference_id: c.id,
                       reference_type: 'Company'})
d3 = Dimension.create({title: "Values", 
                       description: "At Forgeahead, we value 3 E’s: 
Engagement
1)    Our Customers
Forgeahead treats its customers with respect, values their input and is responsive to their needs.
2)    Quality and Excellence
Quality is everyone's responsibility. Forgeahead sets high standards and does not compromise standards for expediency. We take pride in our work and our products.

Expertise
1)    Autonomy, Expertise and Good Judgment
Forgeahead expects individuals to be experts in their jobs and in turn trust their professional judgment. 
2)    Aggressive Goals, Clear Direction and Measured Performance
Strategy is set before challenges by setting high but achievable goals. We measure performances based on goal attainment. Decisions are made in a timely fashion, not only reactively but proactively, anticipating what is needed.
3)    Clear, Candid and Constructive Communication
We are candid when delivering good and bad news. We confront issues early, openly and constructively. When differences arise, focus is on the issue and not the person.

Employee
1)    Personal Involvement and Individual Responsibility
Forgeahead maintains a proper balance between teamwork and individual responsibility. Each person is responsible for working towards the achievement of common company goals. Each person contributes fully, making and meeting commitments. When someone discovers a problem, that person owns it until its rightful owner accepts it.
2)    Individuality and Diversity
Forgeahead values each other as unique individuals and respect each person's differences. We listen to one another's ideas. We respect individual’s time and commitments outside of work. We enjoy a challenging, flexible and fun workplace.
3)    Open Communication
The purpose of the Open Communication Policy is to implement the philosophy that employees are encouraged to raise their work-related concerns informally with their managers or the Human Resources department or with any other manager of their choice. Forgeahead will attempt to keep such expressions of concern, their investigation and their resolution confidential. However it is understood that in the course of investigating and resolving the concerns, some dissemination of information to others may be appropriate.",
                       reference_id: c.id,
                       reference_type: 'Company'})
d4 = Dimension.create({title: "Mission",
                       description: "Primarily to inspire us on our path to success by fostering customer relationships and build products that will drive the future",
                       reference_id: c.id,
                       reference_type: 'Company'})

# [d1, d2, d3, d4].each do |d|
  (1..4).to_a.sample.times do |x|
    Goal.create({title: "Goal #{x+1}", reference_id: c.id, reference_type: 'Company'})
  end
# end

puts "Creating BU/Geo"
b_ind = BusinessUnit.create({title: "FA India", is_headquater: true, address_line_1: "601, Zer01 Business Park", address_line_2: "Mundhwa", city: "Pune", state: 'Maharashtra', zipcode: "411017", country: "India", company_id: c.id, phone: '+1.650.948.1787', fax: '+1.650.948.1789'})
b_usa = BusinessUnit.create({title: "FA US", is_headquater: true, address_line_1: "800 West El Camino Real", address_line_2: "Ste 180, Mountain View", state: 'California', city: "San Francisco", zipcode: "94040", country: "USA", company_id: c.id, phone: '+91 20 40138378', fax: '+91 20 40138499'})

engineering_skills = []
management_skills = []
interests = []
["ruby", "rails", "angular js", "jquery", "css", "bootstrap", "rest", "api", "automation", "rspec", "ruby on rails", "python", "php", "html5", "nginx", "solr", "apache"].each do |skill|
  engineering_skills << Skill.create({title: skill})
end

["people management", "coasting", "estimation", "risk management"].each do |skill|
  management_skills << Skill.create({title: skill})
end

["music", "movies", "drama", "singing", "football", "scricket", "table tanise", "guitar", "swiming", "travelling", "coading", "surfing", "reading", "maditation"].each do |int|
  interests << Interest.create({title: int})
end

male_first_name = ["Abhishek","Bhupendra","Chetan","Chandan", "lareb", "nishant","mahendra", "manik" ,"rakesh","anil", "sunil","irfan", "suhel", "Darshan",  "swapnil", "pankaj","Jeeven", "kaustub", "debosis", "vinod", "jethalal", "atmaram",  "tarak", "gorvindar", "malkit", "surjeet", "mikhkha", "sandeep",
  "Om", "hari", "Jay","mushtaq", "mahesh", "suresh", "ankit", "somnath", "vishwajeet", "zubair","Sushil", "Gajendra", "Shekhar", "rohit", "rahul", "vishal", "vikas"]

female_first_name = ["radhika", "arpita", "bindiya", "charulata", "devika", "emona", "sushma", "sonu", "tina", "piku", "simran", "shyama","Ankita", "Bavna","Charu","Darshana","ambika","kasturi","Kalyani", "kanchan", "toshini","zainab", "yami","sweta","roprekha", "kavita", "nikita","babita", "gunjan", "madhvi","urmi", "somya", "amber", "raziya", "shruti", "payal", "mohini", "priyanka", "shweta", "sneha", "kaynat", "surabhi"]

last_name = ["sinha", "qureshi", "nawab", "sinha", "gupta", "verma", "khan", "jadhav", "ghosh", "sodhi", "mehta", "ghosh","shaikh", "malik", "ranjan", "goud",
 "tiwari", "trivedi", "majumdar", "ojha", "bais", "tiwari", "sharma", "pandye", "shrivastava"]
address_1 = ["shivaji nagar", "fatima nagar", "wanowari", "omax township", "shalimar township", "azad nagar", "juna bazar", "shaniwar peth", "naya bhagh", "main bazar"]
address_2 = ["m.g road", "a. b road", "palasya", "old palasiya", "sarafa", "kanadiya road", "sindhi colony", "rnt marge", "yashvant nagar road", "m r 10", "bhavanrkuan", "navlakha",
  "patnipura", "khajrana", "shri nagar"]


empl_first_name = []
student_father_name = []
student_mother_name = []
prng = Random.new(1234)

puts "--------adding department head ----------"

def create_department_employees(prng, reports_to,male_first_name, last_name,address_1, address_2, d, job_title, index, head = false, team = nil)
    team = team.nil? ? d.teams.sample : team

    params = employee_params(prng, reports_to,male_first_name, last_name, address_2, address_1, d, job_title, index)
    p = Employee.create!(params)
    u = User.create(email: "#{p.first_name}.#{last_name}@forgeahead.io", password: 'password', password_confirmation: 'password', role_id: Role.find_by_name('employee').id, employee_id: p.id)
    p.job_title =  job_title
    p.save

    EmployeeDepartment.create(department_id: d.id, employee_id: p.id, is_department_head: head)
    EmployeeTeam.create(employee_id: p.id, team_id: team.try(:id)) unless team.nil?
    return p
end

def employee_params(prng, reports_to,male_first_name, last_name,address_1, address_2, d, job_title, index)
  mobile = "#{prng.rand(3..9)}#{d.business_unit.id}#{d.id}#{prng.rand(1000000..9999999)}"
  params = {
      first_name: "#{male_first_name}",
      last_name: "#{last_name}",  
      date_of_birth: Date.today - 40.years,
      address_line_1: "#{prng.rand(1..100)}, #{address_1[rand(address_1.length)]}", 
      address_line_2: "#{address_2[rand(address_2.length)]}",
      mobile: mobile, 
      gender: "Male", 
      photo: File.open(Dir["#{Rails.root}/db/images/male/*.jpg"].sample),
      business_unit_id: d.business_unit.id,
      city: d.business_unit.city,
      state: d.business_unit.state,
      country: d.business_unit.country,
      zip: d.business_unit.zipcode,
      joining_date: Date.parse("#{prng.rand(1..28)}-#{prng.rand(1..12)}-#{prng.rand(2008..2015)}"),
      employee_code: "EM#{prng.rand(400..999)}",
      reports_to: reports_to
    }  
end

# INDIA > [d_ind_sales, d_ind_engineering, d_ind_hr, d_ind_admin, d_ind_si]
# US > [d_usa_devops, d_usa_sales, d_usa_engineering]
# JOB TITLE >[jt_dep_hd_opr, jt_dep_hd_fin, jt_dep_hd_si, jt_dep_hd_hr, jt_dep_hd_eng, jt_dep_hd_sale]

puts "Creating Departments for #{b_ind.title}"
d_ind_sales     = Department.create({title: "Sales", business_unit_id: b_ind.id})
d_ind_engineering   = Department.create({title: "Development", business_unit_id: b_ind.id})
d_ind_hr      = Department.create({title: "Human Resources", business_unit_id: b_ind.id})
d_ind_admin     = Department.create({title: "Administration", business_unit_id: b_ind.id})
d_ind_si      = Department.create({title: "System Administration", business_unit_id: b_ind.id})
d_ind_qa      = Department.create({title: "QA", business_unit_id: b_ind.id})
d_ind_fi      = Department.create({title: "Finance & Legal", business_unit_id: b_ind.id})


puts "Creating Departments for #{b_usa.title}"
d_usa_devops    = Department.create({title: "DevOps", business_unit_id: b_usa.id})
d_usa_sales     = Department.create({title: "Sales", business_unit_id: b_usa.id})
d_usa_engineering   = Department.create({title: "Development", business_unit_id: b_usa.id})

bu_ind_departments = b_ind.departments
bu_usa_departments = b_usa.departments

puts "Creating Teams"
#India's Teams in various departments
Team.create({title: 'Sales Team', department_id: d_ind_sales.id})
xen_tm = Team.create({title: 'Xenomorphs (Android - 1)', department_id: d_ind_engineering.id})
min_tm = Team.create({title: 'Minions (Android - 2)', department_id: d_ind_engineering.id})
ids_tm = Team.create({title: 'iDisruptors (iOS - 1)', department_id: d_ind_engineering.id})
fir_tm = Team.create({title: 'Fire-Falcons (iOS - 2)', department_id: d_ind_engineering.id})
Team.create({title: 'Finance Team', department_id: d_ind_hr.id})
Team.create({title: 'HR Team', department_id: d_ind_hr.id})
Team.create({title: 'SI Team', department_id: d_ind_si.id})
Team.create({title: 'Admin and Housekeeping team', department_id: d_ind_admin.id})

#USA Teams in various departments
Team.create({title: 'Sales Team', department_id: d_usa_sales.id})
Team.create({title: 'Sales Team', department_id: d_usa_sales.id})
Team.create({title: 'Oyala Team', department_id: d_usa_engineering.id})
Team.create({title: 'enact deployment Team', department_id: d_usa_engineering.id})

create_department_employees(prng, e_ceo.id,"Mohan", "Kumar", address_1, address_2,d_ind_sales, jt_dep_hd_sale, 1, true)
e_dm = create_department_employees(prng, e_ceo.id,"Zohair", "Hassan",address_1, address_2, d_ind_engineering, jt_dep_hd_eng, 2, true)
create_department_employees(prng, e_ceo.id,"Susmita", "Kaushik",address_1, address_2, d_ind_hr, jt_dep_hd_hr, 3, true)
create_department_employees(prng, e_ceo.id,"Chandan", "Bhagwat",address_1, address_2, d_ind_admin, jt_dep_hd_admin, 4, true)
create_department_employees(prng, e_ceo.id,"Pramod", "Waikar",address_1, address_2, d_ind_si, jt_dep_hd_si, 5, true)
EmployeeDepartment.create(department_id: d_ind_qa.id, employee_id: e_dm.id, is_department_head: true)
create_department_employees(prng, e_ceo.id,"Casmiro", "Aranjo",address_1, address_2, d_ind_fi, jt_dep_hd_fin, 6, true)

EmployeeDepartment.create(department_id: d_usa_devops.id, employee_id: e_ceo.id, is_department_head: true)
EmployeeDepartment.create(department_id: d_usa_sales.id, employee_id: e_ceo.id, is_department_head: true)
EmployeeDepartment.create(department_id: d_usa_engineering.id, employee_id: e_ceo.id, is_department_head: true)




job_titles = [jt1, jt2, jt3, jt4, jt5]

shwetabh_info = {:first_name=>"Shwetabh", :last_name=>"Sharan", :name=>"Shwetabh Sharan", :title=>"System Analyst at Cybage", :summary=>"With more than 7 years of work experience, my objective is to obtain an IT position that utilizes my experience as a developer having responsibility for all aspects of IT including managing software architecture as well as resources.Specialties: Ability to look from business perspective apart from (typical) code-level perspective. Understanding why all the coding is done and where are the fruits for the customer/user whoever it is. Will and ability to self-develop, learn (quickly) new things and adapt to new environments. Good communication skills and a strong emphasis to learn new technologies.Proficient in the following technologies.Python 2.x, Python 3.xFlask 0.10.0, Django1.9 Beta, REST FrameworksHTML 3, CSS, Javascript, AJAX, jQuery, AngularJS 1.3, Kendo UI FrameworkPostgres 9, MySQL, MariaDB, MongoDB, Amazon DynamoDBAmazon EC2, AutoScaling, SOLR Search EngineVim Editor, Git, SVN, Gerrit, Jira, TeamForge, HPQCJenkins, Hudson, CMakeUbuntu 14.04, Centos 7", :location=>"Pune Area", :country=>"India", :industry=>"Information Technology and Services", :picture=>"https://media.licdn.com/mpr/mpr/shrinknp_200_200/p/5/000/1da/031/2a72982.jpg", :skills=>["JavaScript", "Python", "Git", "AJAX", "Django", "PostgreSQL", "CSS", "MySQL", "Linux", "Web Development", "Databases", "Programming", "XML", "HTML", "Ubuntu", "See 9+", "C", "Amazon S3", "Postgres", "Amazon EC2", "HTML / CSS", "EC2", "S3", "AngularJS", "jQuery"], :organizations=>[], :education=>[{:name=>"Bharati Vidyapeeth", :description=>"Bachelor Of Engineering, Computer, 3.5/5Bachelor Of Engineering, Computer, 3.5/5", :degree=>nil, :major=>nil, :period=>"2004 – 2008", :start_date=>"2004", :end_date=>"2008"}, {:name=>"St. Paul's School", :description=>"ICSE, ScienceICSE, Science", :degree=>nil, :major=>nil, :period=>"1997 – 2004", :start_date=>"1997", :end_date=>"2004"}, {:name=>"Carmel Primary School", :description=>"ICSEICSE", :degree=>nil, :major=>nil, :period=>"1990 – 1997", :start_date=>"1990", :end_date=>"1997"}], :websites=>[], :groups=>[], :languages=>[], :certifications=>[], :number_of_connections=>"500+"}
manish_info =  {:first_name=>"Manish", :last_name=>"Pande", :name=>"Manish Pande", :title=>"Sr Software Engineer at Forgeahead Solutions", :summary=>"My Duties Towards My Job:Develops software solutions by studying information needs, conferring with users(client), studying systems flow, data usage, and work processes, investigating problem areas, following the software development lifecycle.Determines operational feasibility by evaluating analysis, problem definition, requirements, solution development, and proposed solutions.Documents and demonstrates solutions by developing documentation, flowcharts, layouts, diagrams, charts, code comments and clear code.Prepares and installs solutions by determining and designing system specifications, standards, and programming.Improves operations by conducting systems analysis, recommending changes in policies and procedures.Updates job knowledge by studying state-of-the-art development tools, programming techniques, and computing equipment, participating in educational opportunities, reading professional publications, maintaining personal networks, participating in professional organizations.Protects operations by keeping information confidential.Provides information by collecting, analyzing, and summarizing development and service issues.Accomplishes engineering and organization mission by completing related results as needed.Supports and develops software engineers by providing advice, coaching and educational opportunities.My Skills/Qualifications: Analysing Information, Software Design, Software Documentation, Software Testing, Teamwork, General Programming Skills, Software Development Fundamentals, Software Development Process, Software Requirements, Software Architecture, Coaching", :location=>"Pune", :country=>"India", :industry=>"Information Technology and Services", :picture=>nil, :skills=>["Java", "Android", "Android Development", "Android SDK", "ADB", "JSON", "XML", "Eclipse", "Programming", "Xcode", "iPhone Application Development", "iPhone Support", "iPad", "Flash", "J2ME Development", "See 8+", "SOAP", "C", "C++", "Core Java", "MySQL", "SQL", "PhoneGap", "Apache Cordova"], :organizations=>[], :education=>[{:name=>"CDAC from Sunbeam Institute", :description=>"Diploma in wireless and Mobile computing, Android, iPhone, ADiploma in wireless and Mobile computing, Android, iPhone, A", :degree=>nil, :major=>nil, :period=>"2012 – 2012", :start_date=>"2012", :end_date=>"2012"}, {:name=>"St vincent palloty nagpur", :description=>"Bachelor of Engineering (BE), Information TechnologyBachelor of Engineering (BE), Information Technology", :degree=>nil, :major=>nil, :period=>"2008 – 2011", :start_date=>"2008", :end_date=>"2011"}, {:name=>"Datta Meghe Polytechnic Nagpur", :description=>"Diploma in Computer Science, Computer ScienceDiploma in Computer Science, Computer Science", :degree=>nil, :major=>nil, :period=>"2006 – 2008", :start_date=>"2006", :end_date=>"2008"}], :websites=>["http://designomediaworks.org/dmw/index.html"], :groups=>[{:name=>"Android Wear Development", :link=>"https://www.linkedin.com/groups?gid=7485234&trk=prof-groups-membership-name"}, {:name=>"Mobile Phone Developer Jobs", :link=>"https://www.linkedin.com/groups?gid=1462037&trk=prof-groups-membership-name"}, {:name=>"Android Wear Testing", :link=>"https://www.linkedin.com/groups?gid=6665249&trk=prof-groups-membership-name"}, {:name=>"Game Developers", :link=>"https://www.linkedin.com/groups?gid=59205&trk=prof-groups-membership-name"}, {:name=>"Google Android Developers", :link=>"https://www.linkedin.com/groups?gid=2055512&trk=prof-groups-membership-name"}, {:name=>"Android Developer Job Opportunities", :link=>"https://www.linkedin.com/groups?gid=3737456&trk=prof-groups-membership-name"}, {:name=>"Android Programming", :link=>"https://www.linkedin.com/groups?gid=3753461&trk=prof-groups-membership-name"}, {:name=>"Information Technology: Jobs, Job Postings & Job Recruiters", :link=>"https://www.linkedin.com/groups?gid=2154029&trk=prof-groups-membership-name"}, {:name=>"Android", :link=>"https://www.linkedin.com/groups?gid=88206&trk=prof-groups-membership-name"}, {:name=>"Android Developer Group", :link=>"https://www.linkedin.com/groups?gid=86481&trk=prof-groups-membership-name"}, {:name=>"India Jobs Network", :link=>"https://www.linkedin.com/groups?gid=1832459&trk=prof-groups-membership-name"}, {:name=>"Mobile Experts ★ Android ★ iPhone", :link=>"https://www.linkedin.com/groups?gid=2013391&trk=prof-groups-membership-name"}, {:name=>"iPad Development Offshore", :link=>"https://www.linkedin.com/groups?gid=2706877&trk=prof-groups-membership-name"}, {:name=>"Google Android", :link=>"https://www.linkedin.com/groups?gid=76373&trk=prof-groups-membership-name"}], :languages=>[], :certifications=>[], :number_of_connections=>"500+"}
sagar_info  = {:first_name=>"Sagar", :last_name=>"Sutar", :name=>"Sagar Sutar", :title=>"Web | Graphic Designer | SEO | PPC", :summary=>nil, :location=>"Pune Area", :country=>"India", :industry=>"Information Technology and Services", :picture=>"https://media.licdn.com/mpr/mpr/shrinknp_200_200/p/1/005/01d/2d2/338901d.jpg", :skills=>["Dreamweaver", "Flash", "Web Design", "Photoshop", "Graphic Design", "SEO", "PPC", "SMO", "GIMP", "Inkscape", "InDesign", "Illustrator", "QuarkXPress", "Pagemaker", "Fireworks Macromedia", "See 6+", "Layout", "Blogging", "User Interface Design", "HTML", "CSS", "Web Development"], :organizations=>[], :education=>[{:name=>"The Brand Saloon, Dadar, Mumbai", :description=>"SEO, PPC, SMO, First ClassSEO, PPC, SMO, First Class", :degree=>nil, :major=>nil, :period=>"2012 – 2013", :start_date=>"2012", :end_date=>"2013"}, {:name=>"Shivaji University", :description=>"Master of Arts (MA), -Master of Arts (MA), -", :degree=>nil, :major=>nil, :period=>"2010 – 2013", :start_date=>"2010", :end_date=>"2013"}, {:name=>"Arena Multimedia Institute, Pune", :description=>"Web, Graphic, Animation Diploma, First ClassWeb, Graphic, Animation Diploma, First Class", :degree=>nil, :major=>nil, :period=>"2008 – 2009", :start_date=>"2008", :end_date=>"2009"}, {:name=>"Dr. N. D. Patil College (Period), Malkapur, Kolhapur", :description=>"Bachelor of Arts (BA), Second ClassBachelor of Arts (BA), Second Class", :degree=>nil, :major=>nil, :period=>"2006 – 2009", :start_date=>"2006", :end_date=>"2009"}, {:name=>"Industrial Training Institute (I. T. I.), Kolhapur.", :description=>"Stenotypist (80 wpm), Government Institute from Kolhapur, AStenotypist (80 wpm), Government Institute from Kolhapur, A", :degree=>nil, :major=>nil, :period=>"2005 – 2006", :start_date=>"2005", :end_date=>"2006"}, {:name=>"Mahatama Gandhi Vidyalaya, Bambawadi, Tal. Shahuwadi, Dist. Kolhapur", :description=>"12th, Art/Art Studies, General, Second Class12th, Art/Art Studies, General, Second Class", :degree=>nil, :major=>nil, :period=>"1997 – 2005", :start_date=>"1997", :end_date=>"2005"}], :websites=>[], :groups=>[], :languages=>[], :certifications=>[], :number_of_connections=>"245"}
sushil_info  = {:first_name=>"Sushil", :last_name=>"Chaudhari", :name=>"Sushil Chaudhari", :title=>"Senior Software Developer at Forgeahead Solutions", :summary=>"- Senior Software Development Experience of more then 4 years in android development. - An experienced android developer proficient in Enterprise Solutions and possess ability to develop unique, cutting edge applications for different handsets and user requirements - Sound knowledge of industry practices and application development protocols - Extensive Technical Consulting Experience with wide range of clients.. - Mentored juniors in Team on Product.", :location=>"Pune Area", :country=>"India", :industry=>"Computer Software", :picture=>nil, :skills=>["Android", "Java", "SQL", "Android Development", "Core Java", "C", "C++", "MySQL", "HTML", "Eclipse", "RESTful WebServices", "Microsoft Office", "Servlets", "XML", "Microsoft Word", "Word", "JSON", "Software Development"], :organizations=>[], :education=>[{:name=>"SKN sighed school of business management", :description=>"Master of Business Administration (MBA), Management Information Systems, General, 60%Master of Business Administration (MBA), Management Information Systems, General, 60%", :degree=>nil, :major=>nil, :period=>"2012 – 2014", :start_date=>"2012", :end_date=>"2014"}, {:name=>"University of Pune", :description=>"Engineer’s Degree, Computer\\, 62.8%Engineer’s Degree, Computer\\, 62.8%", :degree=>nil, :major=>nil, :period=>"2006 – 2011", :start_date=>"2006", :end_date=>"2011"}, {:name=>"Moolji Jaitha Jr. College", :description=>"H.S.C, Computer Science, 68%H.S.C, Computer Science, 68%", :degree=>nil, :major=>nil, :period=>"2004 – 2006", :start_date=>"2004", :end_date=>"2006"}, {:name=>"St.Joseph Convent High School", :description=>"S.S.C, Geology/Earth Science, General, 66.85%S.S.C, Geology/Earth Science, General, 66.85%", :degree=>nil, :major=>nil, :period=>"1991 – 2004", :start_date=>"1991", :end_date=>"2004"}], :websites=>[], :groups=>[{:name=>"Outsource Android Projects", :link=>"https://www.linkedin.com/groups?gid=4350401&trk=prof-groups-membership-name"}, {:name=>"P.V.P.I.T. Pune", :link=>"https://www.linkedin.com/groups?gid=4063421&trk=prof-groups-membership-name"}, {:name=>"Freelance / Contract Android Jobs Europe", :link=>"https://www.linkedin.com/groups?gid=3973247&trk=prof-groups-membership-name"}, {:name=>"Developers - Android, iOS developer , Windows, Java, Ruby, .net, php, django, etc", :link=>"https://www.linkedin.com/groups?gid=54723&trk=prof-groups-membership-name"}, {:name=>"Pune IT Jobs", :link=>"https://www.linkedin.com/groups?gid=916617&trk=prof-groups-membership-name"}, {:name=>"Entrepreneurs Network (4662722)", :link=>"https://www.linkedin.com/groups?gid=4662722&trk=prof-groups-membership-name"}, {:name=>"Google Android Developers", :link=>"https://www.linkedin.com/groups?gid=2055512&trk=prof-groups-membership-name"}, {:name=>"Android Developer Job Opportunities", :link=>"https://www.linkedin.com/groups?gid=3737456&trk=prof-groups-membership-name"}, {:name=>"Current Job Openings in PUNE", :link=>"https://www.linkedin.com/groups?gid=4267464&trk=prof-groups-membership-name"}, {:name=>"Android Internals", :link=>"https://www.linkedin.com/groups?gid=2692131&trk=prof-groups-membership-name"}, {:name=>"Android Programming", :link=>"https://www.linkedin.com/groups?gid=3753461&trk=prof-groups-membership-name"}, {:name=>"Android", :link=>"https://www.linkedin.com/groups?gid=88206&trk=prof-groups-membership-name"}, {:name=>"Android Developer Group", :link=>"https://www.linkedin.com/groups?gid=86481&trk=prof-groups-membership-name"}, {:name=>"Mobile Experts ★ Android ★ iPhone", :link=>"https://www.linkedin.com/groups?gid=2013391&trk=prof-groups-membership-name"}], :languages=>[], :certifications=>[], :number_of_connections=>"500+"}
sunil_info  = {:first_name=>"Sunil", :last_name=>"Lohar", :name=>"Sunil Lohar", :title=>"Software Engineer (iOS and Android)", :summary=>nil, :location=>"Pune", :country=>"India", :industry=>"Computer Software", :picture=>nil, :skills=>["Java", "JavaScript", "iOS development", "Android SDK", "Eclipse", "Android Development", "Android Studio", "SQL", "Xcode", "XML", "jQuery", "AJAX", "PHP", "PhpMyAdmin", "Swift", "Git", "Mobile Applications", "iOS Development"], :organizations=>[], :education=>[{:name=>"Pune University", :description=>"MCA, Computer Software EngineeringMCA, Computer Software Engineering", :degree=>nil, :major=>nil, :period=>"2009 – 2011", :start_date=>"2009", :end_date=>"2011"}], :websites=>[], :groups=>[{:name=>"Small Business Network: Startups & Entrepreneurs talk Social Media Marketing Startup Jobs Sales PR", :link=>"https://www.linkedin.com/groups?gid=1144987&trk=prof-groups-membership-name"}, {:name=>"On Startups - The Community For Entrepreneurs", :link=>"https://www.linkedin.com/groups?gid=2877&trk=prof-groups-membership-name"}, {:name=>"Iphone / Ipad, Android, Blackberry Projects Worldwide", :link=>"https://www.linkedin.com/groups?gid=3500402&trk=prof-groups-membership-name"}], :languages=>[], :certifications=>[], :number_of_connections=>"259"}
anup_info  = {:first_name=>"Anup", :last_name=>"Kulkarni", :name=>"Anup Kulkarni", :title=>"Software QA Engineer at Forgeahead Solutions", :summary=>"I have 2.6 years experience in Quality Assurance domain. During this time I have constantly improved my interpersonal, intellectual and technical skills and still polishing them. Technical point of view, I have worked on both Web and Mobile applications and gained the proficient knowledge on:- Identifying Test Requirements from stories/BRD/FS- Test Cases Creation- Knowledge of API Performance Testing using Jmeter- Web Automation using WebDriver- System, Integration, Regression, Sanity, UI, Compatibility testing for Web and Mobile applications- Mobile Testing on iPhone, Android and Windows for all the versions available till date- Test Cases reviews- Bug tracking tools like JIRA- Hands on automation with Selenium- Version controls using GIT- Agile Methodology- Writing Scripts- Maintenance- OS used Linux, MAC, Windows XP/7/8, Ubuntu", :location=>"Pune Area", :country=>"India", :industry=>"Computer Software", :picture=>nil, :skills=>["C", "Java", "C++", "Ruby", "Manual Testing", "Selenium", "HTML", "SQL", "Microsoft Office", "MySQL", "Microsoft Excel", "JavaScript", "Microsoft Word", "Appium", "Protractor", "See 17+", "Test Automation", "Git", "Regression Testing", "Agile Methodologies", "JIRA", "Test Cases", "Linux", "Testing", "Software Quality Assurance", "Performance Testing", "Android", "Quality Assurance", "Mobile Applications", "Bug Tracking", "Unix", "Jenkins", "JMeter"], :organizations=>[], :education=>[{:name=>"Marathwada Institute of Technology Engineering College, Satara Road.", :description=>"Master's degree, Computer ScienceMaster's degree, Computer Science", :degree=>nil, :major=>nil, :period=>"2011 – 2014", :start_date=>"2011", :end_date=>"2014"}, {:name=>"Vivekanand Arts, Sardar Dalip Singh Commerce & Science College", :description=>"Bachelor of Science (BSc), Computer ScienceBachelor of Science (BSc), Computer Science", :degree=>nil, :major=>nil, :period=>"2008 – 2011", :start_date=>"2008", :end_date=>"2011"}], :websites=>[], :groups=>[], :languages=>[], :certifications=>[], :number_of_connections=>"412"}
lareb_info  = {:first_name=>"Lareb", :last_name=>"Nawab", :name=>"Lareb Nawab", :title=>"Tech Lead at Forgeahead Solutions", :summary=>nil, :location=>"Pune", :country=>"India", :industry=>"Information Technology and Services", :picture=>"https://media.licdn.com/mpr/mpr/shrinknp_200_200/AAEAAQAAAAAAAAPtAAAAJDVhOGQyNDE3LTM2NzEtNGNjMy1iNmRkLTFiZDJjZTc5YTllOA.jpg", :skills=>["Ruby on Rails", "JavaScript", "Ruby", "MySQL", "REST", "jQuery", "AJAX", "CSS", "XML", "Agile Methodologies", "Web Services", "JSON", "Linux", "Java", "HTML", "See 10+", "Java Enterprise Edition", "Neo4j", "NoSQL", "SaaS", "Bootstrap", "jQuery UI", "Spree", "Project Management", "OVP", "AngularJS"], :organizations=>[], :education=>[{:name=>"Holkar college Indore", :description=>"Bachelor of Science (B.Sc.), electronicsBachelor of Science (B.Sc.), electronics", :degree=>nil, :major=>nil, :period=>"2001 – 2004", :start_date=>"2001", :end_date=>"2004"}], :websites=>["http://www.larebnawab.com"], :groups=>[{:name=>"Ruby on Rails SME's", :link=>"https://www.linkedin.com/groups?gid=6591485&trk=prof-groups-membership-name"}], :languages=>[], :certifications=>[], :number_of_connections=>"334"}
neha_info  = {:first_name=>"Neha", :last_name=>"Relan", :name=>"Neha Relan", :title=>"Sr. Software QA Engineer at Vertis Microsystems LLP", :summary=>nil, :location=>"Pune", :country=>"India", :industry=>"Computer Software", :picture=>nil, :skills=>["C++", "C", "Java", "Computer Science", "Algorithms", "SQL", "HTML", "Software Engineering", "Programming", "Microsoft Office", "Microsoft Excel", "Core Java", "Databases", "PowerPoint", "Operating Systems", "See 6+", "Data Structures", "ISTQB Certified", "Microprocessors", "Software Testing Life Cycle", "Manual Testing", "Selenium"], :organizations=>[], :education=>[{:name=>"SSVPS's COE,Dhule", :description=>"Bachelor of Engineering (BE), Computer Science, 65.67%Bachelor of Engineering (BE), Computer Science, 65.67%", :degree=>nil, :major=>nil, :period=>"2005 – 2009", :start_date=>"2005", :end_date=>"2009"}], :websites=>[], :groups=>[], :languages=>[], :certifications=>[], :number_of_connections=>"387"}
shrikant_info = {:first_name=>"Shrikant", :last_name=>"More", :name=>"Shrikant More", :title=>"Sr Engineer Product Development At FIS", :summary=>"Profile :A determined Java developer with good communication skills and ready to work both independently and as a member of a team, utilizing the knowledge and skills acquired throughout my course putting in full of my efforts.Mission and Vision :A career in software engineering position as a Developer that provides opportunities for personal and professional development as well as to contribute to the organization’s effectiveness in order to take the company to the top position along with realizing and exposing my very own potentials.", :location=>"Pune", :country=>"India", :industry=>"Computer Software", :picture=>"https://media.licdn.com/mpr/mpr/shrinknp_200_200/AAEAAQAAAAAAAAjmAAAAJDBmMDhhYmNiLWMwNWItNGU0NS1iMzk2LTdiOWFjODY2MGE5YQ.jpg", :skills=>["Java", "Spring", "Design Patterns", "MySQL", "Hibernate", "Java Enterprise Edition", "JavaScript", "Spring Framework"], :organizations=>[], :education=>[{:name=>"S.I.T.Lonavala", :description=>"BE, ITBE, IT", :degree=>nil, :major=>nil, :period=>"2007 – 2011", :start_date=>"2007", :end_date=>"2011"}, {:name=>"Yashwantrao Chavan Institute Of Science, Satara", :description=>"XIIXII", :degree=>nil, :major=>nil, :period=>"2006 – 2007", :start_date=>"2006", :end_date=>"2007"}], :websites=>[], :groups=>[{:name=>"C / C++ Developers in Israel", :link=>"https://www.linkedin.com/groups?gid=2526482&trk=prof-groups-membership-name"}, {:name=>"R&D Developers in Israel", :link=>"https://www.linkedin.com/groups?gid=2592272&trk=prof-groups-membership-name"}, {:name=>"Engineering Career Opportunities (Jobs & Employment)", :link=>"https://www.linkedin.com/groups?gid=834347&trk=prof-groups-membership-name"}], :languages=>[], :certifications=>[], :number_of_connections=>"190"}
mazhar_info  = {:first_name=>"Mazhar", :last_name=>"Shaikh", :name=>"Mazhar Shaikh", :title=>"Skills", :summary=>nil, :location=>"Pune Area", :country=>"India", :industry=>"Information Technology and Services", :picture=>nil, :skills=>["Business Analysis", "Requirements Specification", "Requirement Specifications", "Requirements Gathering", "Requirements Analysis", "Business Requirements", "Functional Specifications", "Software Project Management", "Business Process Design", "User Acceptance Testing", "Business Process", "SDLC", "Data Modeling", "SharePoint", "Manual Testing", "See 9+", "Visio", "Gap Analysis", "Agile Methodologies", "Systems Analysis", "Business Process Mapping", "PL/SQL", "UML", "SQL", "Team Management"], :organizations=>[], :education=>[{:name=>"University of Pune", :description=>"Pursuing MasterPursuing Master", :degree=>nil, :major=>nil, :period=>"2005 – 2007", :start_date=>"2005", :end_date=>"2007"}, {:name=>"University of Pune", :description=>"2007 Master of Science2007 Master of Science", :degree=>nil, :major=>nil, :period=>nil, :start_date=>nil, :end_date=>nil}, {:name=>"University of Pune", :description=>"2005 Bachelor of Science2005 Bachelor of Science", :degree=>nil, :major=>nil, :period=>nil, :start_date=>nil, :end_date=>nil}], :websites=>[], :groups=>[{:name=>"UAE Career Network", :link=>"https://www.linkedin.com/groups?gid=1026987&trk=prof-groups-membership-name"}, {:name=>"Agile Business Analyst", :link=>"https://www.linkedin.com/groups?gid=1916699&trk=prof-groups-membership-name"}, {:name=>"IIBA (International Institute of Business Analysis)", :link=>"https://www.linkedin.com/groups?gid=92583&trk=prof-groups-membership-name"}, {:name=>"Business Analyst Social Networking", :link=>"https://www.linkedin.com/groups?gid=1870491&trk=prof-groups-membership-name"}, {:name=>"MSBI PROFESSIONALS", :link=>"https://www.linkedin.com/groups?gid=4026581&trk=prof-groups-membership-name"}, {:name=>"Business Analysis Consultants", :link=>"https://www.linkedin.com/groups?gid=2266058&trk=prof-groups-membership-name"}, {:name=>"BA Jobs", :link=>"https://www.linkedin.com/groups?gid=3875990&trk=prof-groups-membership-name"}, {:name=>"Perl Jobs", :link=>"https://www.linkedin.com/groups?gid=1332857&trk=prof-groups-membership-name"}, {:name=>"Business Analyst Professional", :link=>"https://www.linkedin.com/groups?gid=60878&trk=prof-groups-membership-name"}, {:name=>"ModernAnalyst.com - Business Analyst Community", :link=>"https://www.linkedin.com/groups?gid=29008&trk=prof-groups-membership-name"}, {:name=>"Business Analyst Recruiters", :link=>"https://www.linkedin.com/groups?gid=3226695&trk=prof-groups-membership-name"}, {:name=>"Business Analysts", :link=>"https://www.linkedin.com/groups?gid=102710&trk=prof-groups-membership-name"}, {:name=>"Datamatics Global Services", :link=>"https://www.linkedin.com/groups?gid=1789314&trk=prof-groups-membership-name"}, {:name=>"I want to be a PMP®", :link=>"https://www.linkedin.com/groups?gid=2356441&trk=prof-groups-membership-name"}], :languages=>[], :certifications=>[], :number_of_connections=>"483"} 
darshan_info  = {:first_name=>"Darshan", :last_name=>"Zite", :name=>"Darshan Zite", :title=>"Mobile Developer", :summary=>"Android application development, Programmer, software engineering", :location=>"Pune", :country=>"India", :industry=>"Computer Software", :picture=>"https://media.licdn.com/mpr/mpr/shrinknp_200_200/AAEAAQAAAAAAAAj9AAAAJDJiZmVlZTJjLTliOWMtNGVjMy1iZWQ3LWNjYzczNTQxOTk1ZQ.jpg", :skills=>["Android Development", "Core Java", "Java", "Blackberry Development", "SQLite", "J2ME Development", "Eclipse", "HTML", "Microsoft Office", "XML", "RESTful WebServices", "JavaScript", "Android", "JSON", "Team Management", "C", "Blackberry"], :organizations=>[], :education=>[], :websites=>[], :groups=>[{:name=>"Android Developer Group", :link=>"https://www.linkedin.com/groups?gid=86481&trk=prof-groups-membership-name"}, {:name=>"Open Source", :link=>"https://www.linkedin.com/groups?gid=43875&trk=prof-groups-membership-name"}, {:name=>"Google Android", :link=>"https://www.linkedin.com/groups?gid=76373&trk=prof-groups-membership-name"}, {:name=>"Android Academy", :link=>"https://www.linkedin.com/groups?gid=2055495&trk=prof-groups-membership-name"}, {:name=>"On Startups - The Community For Entrepreneurs", :link=>"https://www.linkedin.com/groups?gid=2877&trk=prof-groups-membership-name"}], :languages=>[], :certifications=>[], :number_of_connections=>"390"}

managers_list = [
  {first_name: "Shwetabh", last_name: "Saran", team_id: ids_tm, linkedin_info: shwetabh_info },
  {first_name: "Manish", last_name: "Pande",  team_id: ids_tm, linkedin_info: manish_info },
  {first_name: "Sagar", last_name: "Sutar",  team_id: ids_tm, linkedin_info: sagar_info },

  {first_name: "Sushil", last_name: "Chaudhri", team_id: fir_tm, linkedin_info: sushil_info },
  {first_name: "Sunil", last_name: "Lohar", team_id: fir_tm, linkedin_info: sunil_info },
  {first_name: "Anup", last_name: "Kulkarni", team_id: fir_tm, linkedin_info: anup_info },

  {first_name: "Lareb", last_name: "Nawab", team_id: xen_tm, linkedin_info: lareb_info },
  {first_name: "Neha", last_name: "Rehlan", team_id: xen_tm, linkedin_info: neha_info },
  {first_name: "Shrikant", last_name: "More", team_id: xen_tm,linkedin_info: shrikant_info }, 

  {first_name: "Mazhar", last_name: "Shaikh", linkedin_info: mazhar_info, team_id: min_tm },
  {first_name: "Darshan", last_name: "Zite", linkedin_info: darshan_info, team_id: min_tm }

]

managers = []
managers_list.each_with_index do |emp, index|
  managers << create_department_employees(prng, e_dm.id, emp[:first_name], emp[:last_name],address_1, address_2, d_ind_engineering, jt4, index, false, emp[:team_id])
end

[b_usa, b_ind].each_with_index do |bu, bu_index|
  bu.departments.each_with_index do |depart, d_index|
    depart.teams.each_with_index do |team, t_index|
      puts "Creating employees under #{bu.title} > #{depart.title} > #{team.title}...."
      5.times do |index|
        #student first and last name
        empl_first_name = "#{male_first_name[rand(male_first_name.length)]}"
        empl_last_name = "#{last_name[rand(last_name.length)]}"
        photo =  File.open(Dir["#{Rails.root}/db/images/male/*.jpg"].sample)
        dob = Date.parse("#{prng.rand(1..28)}-#{prng.rand(1..12)}-#{prng.rand(1970..1995)}")
        joining_date = Date.parse("#{prng.rand(1..28)}-#{prng.rand(1..12)}-#{prng.rand(2008..2015)}")
        # puts "#{index + 1}-----------#{empl_first_name} #{empl_last_name}"
        code = "EM#{bu_index}#{d_index}#{t_index}#{index}"
        p = Employee.create(first_name: empl_first_name,last_name: empl_last_name, date_of_birth: dob, employee_code: code, joining_date: joining_date, mobile: (9876543000+index).to_s, gender: "Male", photo: photo, 
                :address_line_1 => "#{prng.rand(1..100)}, #{address_1[rand(address_1.length)]}", :address_line_2 => "#{address_2[rand(address_2.length)]}", :city => "Pune", :state => "Maharashtra", :zip => "411013",
                  country: "India", :mobile => "#{prng.rand(21528950..29999999)}", office_phone_ext: "#{490+index}", office_phone: "#{prng.rand(21528950..29999999)}", business_unit_id: bu.id, reports_to: managers.sample.try(:id))
        u = User.create(email: "#{empl_first_name}.#{empl_last_name}@forgeahead.io", password: 'password', password_confirmation: 'password', role_id: Role.find_by_name('employee').id, employee_id: p.id)

        EmployeeDepartment.create(employee_id: p.id, department_id: depart.id)
        EmployeeTeam.create(employee_id: p.id, team_id: team.id)

        p.job_title =  job_titles[rand(job_titles.length)]
        p.save

        prng.rand(2..8).times do |index|
          #if managers (PM or HR)
          if [jt4.id, jt5.id].include?(p.job_title_id)
            EmployeeSkill.create(employee_id: p.id, skill_id: "#{management_skills[rand(management_skills.length)].try(:id)}", is_primary: [true, false].sample)
          else
            EmployeeSkill.create(employee_id: p.id, skill_id: "#{engineering_skills[rand(engineering_skills.length)].try(:id)}", is_primary: [true, false].sample)
          end      
        end

        prng.rand(2..4).times do |index|
          EmployeeInterest.create({employee_id: p.id, interest_id: "#{interests[rand(interests.length)].try(:id)}"})
        end
        #--------------------------------------------------------------------------------------
        
        #FEMALE Employees  
        empl_first_name = "#{female_first_name[rand(female_first_name.length)]}"
        empl_last_name = "#{last_name[rand(last_name.length)]}"
        photo =  File.open(Dir["#{Rails.root}/db/images/female/*.jpg"].sample)
        code = "F#{bu_index}#{d_index}#{t_index}#{index}"
        # puts "#{index + 1}-----------#{empl_first_name} #{empl_last_name}"
        
        pf = Employee.create(first_name: empl_first_name,last_name: empl_last_name, date_of_birth: dob, employee_code: code, joining_date: joining_date, mobile: (9876543000+index).to_s, gender: "Male", photo: photo, 
                :address_line_1 => "#{prng.rand(1..100)}, #{address_1[rand(address_1.length)]}", :address_line_2 => "#{address_2[rand(address_2.length)]}", :city => "Pune", :state => "Maharashtra", :zip => "411013",
                  country: "India", :mobile => "#{prng.rand(21528950..29999999)}", office_phone_ext: "#{490+index}", office_phone: "#{prng.rand(21528950..29999999)}", business_unit_id: bu.id)

        u = User.create(email: "#{empl_first_name}.#{empl_last_name}@forgeahead.io", password: 'password', password_confirmation: 'password', role_id: Role.find_by_name('employee').id, employee_id: pf.id) unless p.nil?
        
        EmployeeDepartment.create(employee_id: pf.id, department_id: depart.id)
        EmployeeTeam.create(employee_id: pf.id, team_id: team.id)

        pf.job_title =  job_titles[rand(job_titles.length)]
        pf.save

        prng.rand(2..8).times do |index|
          if [jt4.id, jt5.id].include?(pf.job_title_id)
            EmployeeSkill.create(employee_id: pf.id, skill_id: "#{management_skills[rand(management_skills.length)].try(:id)}")
          else
            EmployeeSkill.create(employee_id: pf.id, skill_id: "#{engineering_skills[rand(engineering_skills.length)].try(:id)}")
          end      
        end

        prng.rand(2..4).times do |index|
          EmployeeInterest.create({employee_id: pf.id, interest_id: "#{interests[rand(interests.length)].try(:id)}"})
        end        
      end
    end
  end
end

puts "#{Employee.all.count} employees added---"

MessageCategory.create([{name: 'Suggestion'}, {name: 'Complaint'}, {name: 'Improvement'}, {name: 'Idea'}])

["Datviet", "Oyala Release", "Just for friends"].each do |chat_room|
   cr = ChatRoom.create({title: chat_room, employee_id: Employee.first.id})
   (4..12).to_a.sample.times do |index|
      ChatRoomMember.create(chat_room_id: cr.id, employee_id: Employee.all.sample.id)
   end
end

