ERROR_CODES = HashWithIndifferentAccess.new(YAML::load(File.open("#{Rails.root}/config/error_codes.yml")))

UNREAD = 'Unread'
ONHOLD = 'Onhold'
REJECTED = "Rejected"
ACCEPTED = "Accepted"
