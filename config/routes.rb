Rails.application.routes.draw do  
  resources :dimensions
  resources :goals
  resources :message_categories
  resources :messages, except: [:index, :destroy] do
    collection do
      get :sent_messages
      get :received_messages
    end
  end

  resources :posts
  devise_for :users, :controllers => {sessions: 'sessions', registrations: 'registrations'} 
  resources :employees do
    resources :dimensions
    resources :goals
    collection do
      post :search
      get :my_profile
    end
    member do 
      get :linked_in_profile
    end
    resources :users, only: [:new, :create, :edit, :update, :destroy]
  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  resources :job_titles
  resources :roles, only: [:index] do
    resources :dimensions
  end
  resources :companies do 
    resources :dimensions
    resources :goals
    resources :business_units do      
      resources :departments do
        resources :teams
      end
    end
  end

  resources :business_units do
    resources :dimensions
  end

  resources :teams do
    resources :dimensions
    resources :goals
  end

  resources :chat_rooms do
    member do
      delete :leave
    end
    # resources :chat_room_members, path: :members
    resources :chat_messages do
      collection do
        delete :clear_chat_history, to: "chat_messages#clear_chat_history"
      end
    end
  end


  mount ActionCable.server => '/cable'

  
  root to: "home#index"

  get '/api' => redirect('/swagger/dist/index.html?url=/apidocs/api-docs.json')

end
