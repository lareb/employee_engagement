require 'test_helper'

class ChatRoomMembersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @chat_room_member = chat_room_members(:one)
  end

  test "should get index" do
    get chat_room_members_url
    assert_response :success
  end

  test "should get new" do
    get new_chat_room_member_url
    assert_response :success
  end

  test "should create chat_room_member" do
    assert_difference('ChatRoomMember.count') do
      post chat_room_members_url, params: { chat_room_member: {  } }
    end

    assert_redirected_to chat_room_member_url(ChatRoomMember.last)
  end

  test "should show chat_room_member" do
    get chat_room_member_url(@chat_room_member)
    assert_response :success
  end

  test "should get edit" do
    get edit_chat_room_member_url(@chat_room_member)
    assert_response :success
  end

  test "should update chat_room_member" do
    patch chat_room_member_url(@chat_room_member), params: { chat_room_member: {  } }
    assert_redirected_to chat_room_member_url(@chat_room_member)
  end

  test "should destroy chat_room_member" do
    assert_difference('ChatRoomMember.count', -1) do
      delete chat_room_member_url(@chat_room_member)
    end

    assert_redirected_to chat_room_members_url
  end
end
