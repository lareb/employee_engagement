module ChatRoomsHelper
	def members_link(chat_room)
		chat_room.members.each do |m|
			link_to m.first_name, employee_path(m)
		end
	end
end
