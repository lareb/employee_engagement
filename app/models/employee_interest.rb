class EmployeeInterest < ApplicationRecord
	belongs_to :employee
	belongs_to :interest

	validates :interest_id, :uniqueness => {scope: :employee_id}
end
