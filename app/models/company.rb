# Company Model
class Company < ApplicationRecord
  has_many :business_units
  has_many :dimensions, as: :reference
  has_many :goals, as: :reference

  has_attached_file :logo, styles: { small: '40x40#', medium: '80x80#', large: '160x160#' }, default_url: '/images/missing_:style.png'
  validates_attachment_content_type :logo, :content_type => /\Aimage\/.*\Z/

  validates :name, uniqueness: :true
end
