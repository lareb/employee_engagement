# Dimension Model
class Dimension < ApplicationRecord  
  belongs_to :reference, polymorphic: true

  validates :title, presence: true

  def self.dimensions_mapping
   {'companies' => 'company_id',
   	'employees' => 'employee_id',
   	'roles' => 'role_id',
   	'business_units' => 'business_unit_id',
   	'teams' => 'team_id' 
  			}
  end
end
