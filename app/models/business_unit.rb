# Business Unit Model
class BusinessUnit < ApplicationRecord
  belongs_to :company
  has_many :departments
  has_many :employees
  has_many :dimensions, as: :reference

  validates :title, uniqueness: :true
  validates :address_line_1, :city, :country, :phone, :fax, presence: true
  def address
    [
      address_line_1.try(:capitalize),
      address_line_2.try(:capitalize),
      city.try(:capitalize),
      state.try(:capitalize),
      zipcode.try(:capitalize)
    ].reject { |n| n.blank? }.join(', ')
  end
end
