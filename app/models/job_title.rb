class JobTitle < ApplicationRecord
	has_one :employee
	validates :title, :uniqueness => true
end
