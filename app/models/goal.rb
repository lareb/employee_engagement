class Goal < ApplicationRecord
  belongs_to :reference, polymorphic: true
  # validates :title, :uniqueness => {scope: :dimension_id}

  def self.goals_mapping
   {'companies' => 'company_id',
   	'employees' => 'employee_id',   	
   	'teams' => 'team_id' 
  	}
  end
end
