class ChatRoomMember < ApplicationRecord
	belongs_to :employee
	belongs_to :chat_room

	validates :employee_id, presence: true
	validates :employee_id, uniqueness: {scope: :chat_room_id}
end
