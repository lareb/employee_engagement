class Department < ApplicationRecord
	belongs_to :business_unit
	has_many :employee_departments
	has_many :employees, through: :employee_departments

	has_many :teams

	validates :title, :uniqueness => {scope: :business_unit_id}
	validates :business_unit_id, :presence => true

	def head
		employee_departments.where(is_department_head: true).first.try(:employee)
	end

end
