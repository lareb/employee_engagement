class Team < ApplicationRecord
	has_many :employee_teams
	has_many :employees, through: :employee_teams
	has_many :dimensions, as: :reference
	has_many :goals, as: :reference
	belongs_to :department

	validates :title, :uniqueness => {scope: :department_id}
	# validates :department_id, :presence => true
end
