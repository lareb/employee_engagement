class Ability
    include CanCan::Ability

    def initialize(user)
        # Define abilities for the passed in user here. For example:
        #
        user ||= User.new # guest user (not logged in)
        if user.admin?
            can :manage, :all

            can :access, :rails_admin   # grant access to rails_admin
            can :dashboard              # grant access to the dashboard        
        end

        if user.employee?
            can :read, [Company, BusinessUnit, Department, Team, Role]
            can :read, Employee
            can :search, Employee
            can :my_profile, Employee
            can :update, Employee, id: user.employee.id

            #employee can create new chat rooms
            create_new_chat_room(user)
            #can get all chat room where he/she belongs
            list_of_user_chat_rooms(user)
            #creator only can delete chat rooms
            delete_chat_room(user)
            #member of any chat group can send message to his/hher group
            send_message_to_chat(user)
            #any one chan leave chat group
            leave_chat_group(user)
            #can read message category (anynomous message)
            can_read_message_category
            #employee can send any. message
            can_create_message(user)
            #can read message sent by him/her recieved by him/her
            read_messages(user)
            #can update message
            can_update_feedback_message(user)
            #can update chat rooms, member or chat room name
            update_chat_room_detail(user)
            #can read dimension and goals of all entity
            can_read_dimensions_and_goals
            #
            can_read_linked_profile()
        end
    end


    def can_update_feedback_message(user)
        can :update, Message, recipient_id: user.employee.id
    end
    #Let's categories the ability into complete action

    def can_read_dimensions_and_goals
        can :read, Dimension
        can :read, Goal
    end

    def create_new_chat_room(user)
        can :create, ChatRoom
        can :read, ChatRoom, id: user.employee.chat_rooms.map(&:id)
    end

    #can update chat room title
    #can update members of chat room
    def update_chat_room_detail(user)
        can :update, ChatRoom, id: user.employee.chat_rooms.map(&:id)
    end

    def list_of_user_chat_rooms(user)
        can :index, ChatRoom
    end

    #only creator of channel can delete chat group
    def delete_chat_room(user)
        can :delete, ChatRoom, employee_id: user.employee.id
    end

    def send_message_to_chat(user)
        can :manage, ChatMessage, chat_room_id: user.employee.chat_rooms.map(&:id)
    end

    def leave_chat_group(user)
        can :leave, ChatRoom, id: user.employee.chat_rooms.map(&:id)
    end

   def can_read_message_category
        can :read, MessageCategory
   end

    def can_create_message(user)
        can :create, Message
    end

    def read_messages(user)
        can :read, Message, sender_id: user.employee.try(:id)
        can :read, Message, recipient_id: user.employee.try(:id)
        can :sent_messages, Message, sender_id: user.employee.try(:id)
        can :received_messages, Message, recipient_id: user.employee.try(:id)
    end
    
    def can_read_linked_profile
        can :linked_in_profile, Employee
    end     
end


