class Message < ApplicationRecord
	belongs_to :message_category
	belongs_to :sender, class_name: 'Employee', optional: true
	belongs_to :recipient, class_name: 'Employee'
	validates :message_category_id, :recipient_id, :body, presence: true
	validate :status_updation, on: :update

  	validate :check_if_message_anonymous, on: :create

	def check_if_message_anonymous
		if anonymous && !sender_id.nil?
			errors.add(:sender_id, "information is not acceptable because message is anonymous")
		end
	end

	def status_updation
		case status_was
		when UNREAD
			if status.eql? UNREAD
				errors.add(:status, "Received status #{status} is not "\
					"valid after current status #{status_was}")
			end
		when ONHOLD
			if [UNREAD, ONHOLD].include? status
				errors.add(:status, "Received status #{status} is not "\
					"valid after current status #{status_was}")
			end
		else
			errors.add(:status, "Received status #{status} is not "\
				"valid after current status #{status_was}")
		end
	end
end
