class User < ApplicationRecord
  # acts_as_token_authenticatable
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable


	belongs_to :employee, optional: true
	belongs_to :role

  # has_many :chat_rooms, dependent: :destroy
  # has_many :chat_messages, dependent: :destroy

  before_save :ensure_authentication_token

  validates :employee_id, :role_id, presence: true
  validate :user_account_exist, on: :create

  def user_account_exist
    unless User.find_by_employee_id(employee_id).nil?
      errors.add(:user, "account already exist")
    end
  end

  def admin?
    role.try(:name) == 'admin'
  end

  def employee?
    role.try(:name) == 'employee'
  end

  def ensure_authentication_token
    if authentication_token.blank?
      self.authentication_token = generate_authentication_token
    end
  end

  def name
    employee.try(:first_name) || email
  end

  private
  
  def generate_authentication_token
    loop do
      token = Devise.friendly_token
      break token unless User.where(authentication_token: token).first
    end
  end
end
