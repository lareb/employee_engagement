class EmployeeTeam < ApplicationRecord
	belongs_to :employee
	belongs_to :team

	validates :team_id, :uniqueness => {scope: :employee_id}
end
