class Post < ApplicationRecord
	belongs_to :user

  	validates :content, :presence => true, :length => { :minimum => 20 }
	validates :title, :presence => true
	validates :name, :presence => true
end
