# Employee Model
class Employee < ApplicationRecord
  attr_accessor :primary_skills_params, :secondary_skills_params,
                :interests_params

  has_one :user
  belongs_to :business_unit

  belongs_to :reports_to_manager, :class_name => 'Employee', :foreign_key => 'reports_to'
  has_many :reportees, class_name: 'Employee', :foreign_key => "reports_to"

  has_many :employee_departments
  has_many :departments, through: :employee_departments

  has_many :employee_teams
  has_many :teams, through: :employee_teams

  has_many :employee_skills
  has_many :skills, through: :employee_skills

  has_many :primary_skills, -> { where('employee_skills.is_primary is true') }, through: :employee_skills, source: :skill
  has_many :secondary_skills, -> { where('employee_skills.is_primary is not true') }, through: :employee_skills, source: :skill

  has_many :employee_interests
  has_many :interests, through: :employee_interests

  # has_many :chat_rooms, dependent: :destroy
  has_many :created_chat_rooms, class_name: ChatRoom, foreign_key: :employee_id
  has_many :chat_messages, dependent: :destroy

  has_many :chat_room_members
  has_many :chat_rooms, through: :chat_room_members

  belongs_to :job_title
  has_many :dimensions, as: :reference
  has_many :goals, as: :reference

	has_many :sent_messages, class_name: 'Message', foreign_key: :sender_id
	has_many :received_messages, class_name: 'Message', foreign_key: :recipient_id

	has_many :received_anonymous_messages, -> { where("messages.anonymous is true")}, class_name: 'Message', foreign_key: :recipient_id
	has_many :received_nonanonymous_messages, -> { where("messages.anonymous is false")}, class_name: 'Message', foreign_key: :recipient_id
	



  
	validates :first_name, :date_of_birth, :employee_code, :joining_date, :mobile, :gender, :presence => true
	validates :employee_code, :mobile , uniqueness: true

  has_attached_file :photo, styles: { small: '40x40#', medium: '80x80#', large: '160x160#' }, default_url: '/images/missing_:style.png'
  validates_attachment_content_type :photo, :content_type => /\Aimage\/.*\Z/

  accepts_nested_attributes_for :employee_departments
  accepts_nested_attributes_for :employee_teams
  accepts_nested_attributes_for :interests

  def full_name
    [first_name.try(:capitalize), middle_name.try(:capitalize), last_name.try(:capitalize)].reject {|n| n.blank? }.join(" ")	
  end

  private

  def self.search(params)
    include_models = []
    where_condition = {}

    query = params[:business_unit]
    unless query.blank?
      include_models << [:business_unit]
      where_condition.merge!({ business_units: { title: query } })
    end

    query = params[:business_unit_ids]
    unless query.blank?
      raise 'Invalid request object, business_unit_ids should be an array' unless query.is_a?(Array)
      include_models << [:business_unit]
      where_condition.merge!({ business_units: { id: query } })
    end

    query = params[:departments]
    unless query.blank?
      include_models << [:employee_departments, :departments]
      where_condition.merge!({ departments: { title: query } })
    end

    query = params[:department_ids]
    unless query.blank?
      raise 'Invalid request object, department_ids should be an array' unless query.is_a?(Array)
      include_models << [:employee_departments, :departments]
      where_condition.merge!({ departments: { id: query } })
    end

    query = params[:teams]
    unless query.blank?
      include_models << [:employee_teams, :teams]
      where_condition.merge!({ teams: { title: query } })
    end

    query = params[:team_ids]
    unless query.blank?
      raise 'Invalid request object, team_ids should be an array' unless query.is_a?(Array)
      include_models << [:employee_teams, :teams]
      where_condition.merge!({ teams: { id: query } })
    end

    query = params[:skills]
    unless query.blank?
      include_models << [:employee_skills, :skills]
      where_condition.merge!({ skills: { title: query } })
    end

    query = params[:interests]
    unless query.blank?
      include_models << [:employee_interests, :interests]
      where_condition.merge!({ interests: { title: query } })
    end
    include_models.uniq!
    query = params[:name]
    if query.blank?
      Employee.includes(include_models).where(where_condition)
    else
      Employee.includes(include_models).where(where_condition).where("first_name ILIKE ? OR last_name ILIKE ? OR middle_name ILIKE ? ", "%#{query}%", "%#{query}%", "%#{query}%")
    end
  end
end
