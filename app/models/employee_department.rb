class EmployeeDepartment < ApplicationRecord
	belongs_to :employee
	belongs_to :department

	validates :department_id, :uniqueness => {scope: :employee_id}
	validate :validate_for_department_head


	def validate_for_department_head
		if is_department_head
			errors.add("", "Department head is already exist (#{department.head.full_name}) for this department") unless department.head.blank?
		end
	end
end
