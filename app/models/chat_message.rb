class ChatMessage < ApplicationRecord
  belongs_to :employee
  belongs_to :chat_room

  validates :body, presence: true, length: {minimum: 2, maximum: 1000}

  validate :validate_for_membership
  after_create_commit { ChatMessageBroadcastJob.perform_later(self) }

  def timestamp
    created_at.strftime('%H:%M:%S %d %B %Y')
  end

  def validate_for_membership
  	unless employee.chat_rooms.find_by_id(chat_room_id)
  		errors.add(:chat_room_id, "not allowed")
  	end
  end
end
