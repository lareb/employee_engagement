class ChatRoom < ApplicationRecord
	attr_accessor :target_employee

  	belongs_to :created_by, class_name: Employee, foreign_key: :employee_id
  	
  	has_many :chat_room_members, dependent: :destroy
  	has_many :members, through: :chat_room_members ,class_name: Employee, foreign_key: :employee_id, :source => :employee

  	has_many :chat_messages, dependent: :destroy

  	validates :title, :uniqueness => true

  	after_create :create_members

    accepts_nested_attributes_for :members

  	private
  	def create_members
  		chat_room_members.create(employee_id: created_by.id)
  	end
end
