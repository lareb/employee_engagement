# Role Model
class Role < ActiveRecord::Base
  has_one :role
  has_many :dimensions, as: :reference

  validates :title, :name, uniqueness: :true
end
