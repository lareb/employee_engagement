class ChatMessageBroadcastJob < ApplicationJob
  queue_as :default

  def perform(message)
    ActionCable.server.broadcast "chat_rooms_#{message.chat_room.id}_channel",
                                 message: render_chat_message(message)
  end

  private

  def render_chat_message(message)
    #for web
    # ChatMessagesController.render partial: 'chat_messages/chat_message', locals: {chat_message: message, index: 1}
  	ChatMessagesController.render partial: 'chat_messages/message', locals: {chat_message: message, index: 1}
  end
end