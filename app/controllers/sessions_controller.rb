class SessionsController < Devise::SessionsController  
    
	skip_before_filter :verify_authenticity_token, :if => Proc.new { |c| c.request.format == 'application/json' }
	respond_to :json
	
	swagger_controller :sessions, "Login and Logout"
 
	  #Add this block
	  swagger_api :create do
	    summary "Login"
	    format "json"
	    param :form, :email, :string, :required, "Email address"
	    param :form, :password, :string, :required, "Password"
	    response :unauthorized
	    response :not_acceptable
	  end
	 
	  swagger_api :destroy do
	    summary "Logout"
	    param :path, :id, :integer, :required, "User ID"
	    response :unauthorized
	    response :not_acceptable
	  end

end 

