class DepartmentsController < ApplicationController
  before_action :authenticate_user!

  load_and_authorize_resource :company
  load_and_authorize_resource :business_unit, :through => :company
  load_and_authorize_resource :department, :through => :business_unit

  before_action :set_breadcrumb

  swagger_controller :departments, "Department Managment"

  swagger_api :create do
    summary "Creates a new Department"
    param :form, "department[title]", :string, :required, "Title"
    param :form, "department[description]", :text, :optional, "Description"
    response :unauthorized
    response :not_acceptable
  end
  
  swagger_api :update do
    summary "Update Department"
    param :form, "department[title]", :string, :required, "Title"
    param :form, "department[description]", :text, :optional, "Description"
    response :unauthorized
    response :not_acceptable
  end

  def index
    add_breadcrumb "Departments"
    # @departments = @business_unit.departments
  end

  # GET /departments/1
  # GET /departments/1.json
  def show
  end

  # GET /departments/new
  def new
    add_breadcrumb "Departments", company_business_unit_departments_path(@company, @business_unit)
    add_breadcrumb "Add New Departments"
    # @department = Department.new
  end

  # GET /departments/1/edit
  def edit
    add_breadcrumb "Departments", company_business_unit_departments_path(@company, @business_unit)
    add_breadcrumb "Edit #{@department.title}"
  end

  # POST /departments
  # POST /departments.json
  def create
    @department = @business_unit.departments.build(department_params)
    respond_to do |format|
      if @department.save
        format.html { redirect_to new_company_business_unit_department_team_path(@company, @business_unit, @department), notice: 'Department was successfully created.' }
        format.json { render :show, status: :created, location: @department }
      else
        format.html { render :new }
        format.json { render json: @department.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /departments/1
  # PATCH/PUT /departments/1.json
  def update
    respond_to do |format|
      if @department.update(department_params)
        format.html { redirect_to company_business_unit_department_teams_path(@company, @business_unit, @department), notice: 'Department was successfully updated.' }
        format.json { render :show, status: :ok, location: @department }
      else
        format.html { render :edit }
        format.json { render json: @department.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /departments/1
  # DELETE /departments/1.json
  def destroy
    @department.destroy
    respond_to do |format|
      format.html { redirect_to company_business_unit_department_teams_path(@company, @business_unit, @department), notice: 'Department was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_breadcrumb
      add_breadcrumb @company.name, company_path(@company)
      add_breadcrumb "Business Units", company_business_unit_path(@company, @business_unit)
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def department_params
      params.fetch(:department, {}).permit([:title, :description])
    end
end
