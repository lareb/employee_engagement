class DimensionsController < ApplicationController
  # before_action :set_dimension, only: [:show, :edit, :update, :destroy]
  before_action :set_dimension_reference

  before_action :authenticate_user!
  load_and_authorize_resource
  
  # GET /dimensions
  # GET /dimensions.json
  def index    
    if @ref_id.blank?
      @dimensions = Dimension.all
    else
      @dimensions = Dimension.where(reference_id: @ref_id, reference_type: @ref_type)
    end
  end
  # GET /dimensions/1
  # GET /dimensions/1.json
  def show    
    @dimension = Dimension.where(id: params[:id], reference_id: @ref_id, reference_type: @ref_type).first    
    respond_to do |format|
      if @dimension.blank?
        format.json { render json: 'Record Not found', status: :unprocessable_entity }
      else
        format.json { render :show, status: :created, location: @dimension }
      end
    end
  end

  # GET /dimensions/new
  def new
    @dimension = Dimension.new
  end

  # GET /dimensions/1/edit
  def edit
  end

  # POST /dimensions
  # POST /dimensions.json
  def create
    @dimension = Dimension.new(dimension_params.merge(reference_id: @ref_id, reference_type: @ref_type))

    respond_to do |format|
      if @dimension.save
        # format.html { redirect_to @dimension, notice: 'Dimension was successfully created.' }
        format.json { render :show, status: :created, location: @dimension }
      else
        # format.html { render :new }
        format.json { render json: @dimension.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /dimensions/1
  # PATCH/PUT /dimensions/1.json
  def update
    respond_to do |format|
      if @dimension.update(dimension_params.merge(reference_id: @ref_id, reference_type: @ref_type))
        # format.html { redirect_to @dimension, notice: 'Dimension was successfully updated.' }
        format.json { render :show, status: :ok, location: @dimension }
      else
        # format.html { render :edit }
        format.json { render json: @dimension.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /dimensions/1
  # DELETE /dimensions/1.json
  def destroy
    @dimension.destroy
    respond_to do |format|
      # format.html { redirect_to dimensions_url, notice: 'Dimension was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_dimension
      @dimension = Dimension.find(params[:id])
    end

    def set_dimension_reference      
      ref = request.path.split('/')      
      @ref_type = ref[1].singularize.classify      
      @ref_id = params[Dimension.dimensions_mapping[ref[1]]]
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def dimension_params
      params.require(:dimension).permit(:id, :title, :description)
    end
end
