class ApplicationController < ActionController::Base
    # acts_as_token_authentication_handler_for User, fallback: :none
  	protect_from_forgery #with: :exception

    # before_filter :authenticate_user_from_token!
    # # This is Devise's authentication
    # before_filter :authenticate_user!    

  	add_breadcrumb "Home", :root_path

    ######################SWAGGER API DOCS#####################
    class << self
    Swagger::Docs::Generator::set_real_methods

    def inherited(subclass)
      super
      return unless [CompaniesController, BusinessUnitsController, DepartmentsController, TeamsController, EmployeesController].include?(subclass)
      subclass.class_eval do
        setup_basic_api_documentation(subclass)
      end
    end

    private
    def setup_basic_api_documentation(subclass)
      [:index, :show, :create, :update, :delete].each do |api_action|
        swagger_api api_action do
          param :header, 'user-email', :string, :required, 'User email'
          param :header, 'user-token', :string, :required, 'Authentication-Token'

          param :path, :company_id, :integer, :required, "company id" if [BusinessUnitsController, DepartmentsController, TeamsController].include?(subclass)
          param :path, :business_unit_id, :integer, :required, "business unit id" if [DepartmentsController, TeamsController].include?(subclass)
          param :path, :department_id, :integer, :required, "department id" if [TeamsController].include?(subclass)

          param :path, :id, :integer, :required, "id"
        end
      end
    end
    end
    ######################SWAGGER API DOCS#####################



  	rescue_from CanCan::AccessDenied do |exception|
      respond_to do |format|
        format.html {redirect_to main_app.root_path, :alert => exception.message}
        format.json { render json: generate_error(:not_authorized, exception.message) }
      end      
  	end  	

    rescue_from(ActiveRecord::RecordNotFound) do |exception|
      respond_to do |format|
        format.html {redirect_to main_app.root_path, :alert => exception.message}
        format.json { render json: generate_error(:bad_request, exception.message) }
      end       
    end

    def current_employee
    	current_user.employee
  	end

  	def current_company
  		Company.first
  	end

    def generate_error(type, error_message = nil)
      {
        :status  =>  "error",
        :message =>  error_message || ERROR_CODES[type][:message],
        code: ERROR_CODES[type][:code]
      }
    end

    def format_errors errors
      errors.map{|k,v| "#{k.to_s.humanize} #{v}"}
    end

  private

  def authenticate_user!
      if request.format.json?
        authenticate_user_from_token!
        return      
      end
      super
  end
  
  def authenticate_user_from_token!
    user_email = request.headers["user-email"].presence
    user_token = request.headers["user-token"]

    # user_email = params[:user_email].presence
    user       = user_email && User.find_by_email(user_email)
    # Notice how we use Devise.secure_compare to compare the token
    # in the database with the token given in the params, mitigating
    # timing attacks.
    if user && Devise.secure_compare(user.authentication_token, user_token)
      sign_in user, store: false
    else
      render json: {error: 300, message: "Invalid credentials"}
    end
  end

end
