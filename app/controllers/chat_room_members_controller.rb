class ChatRoomMembersController < ApplicationController
  # before_action :authenticate_user!

  # load_and_authorize_resource :chat_room
  # load_and_authorize_resource :chat_room_members, :through => :chat_room

  # # GET /chat_room_members
  # # GET /chat_room_members.json
  # def index
    
  # end

  # # GET /chat_room_members/1
  # # GET /chat_room_members/1.json
  # def show
  # end

  # # GET /chat_room_members/new
  # def new
  #   @chat_room_member = ChatRoomMember.new
  # end

  # # GET /chat_room_members/1/edit
  # def edit
  # end

  # # POST /chat_room_members
  # # POST /chat_room_members.json
  # def create
  #   @chat_room_member = ChatRoomMember.new(chat_room_member_params)

  #   respond_to do |format|
  #     if @chat_room_member.save
  #       format.html { redirect_to @chat_room_member, notice: 'Chat room member was successfully created.' }
  #       format.json { render :show, status: :created, location: @chat_room_member }
  #     else
  #       format.html { render :new }
  #       format.json { render json: @chat_room_member.errors, status: :unprocessable_entity }
  #     end
  #   end
  # end

  # # PATCH/PUT /chat_room_members/1
  # # PATCH/PUT /chat_room_members/1.json
  # def update
  #   respond_to do |format|
  #     if @chat_room_member.update(chat_room_member_params)
  #       format.html { redirect_to @chat_room_member, notice: 'Chat room member was successfully updated.' }
  #       format.json { render :show, status: :ok, location: @chat_room_member }
  #     else
  #       format.html { render :edit }
  #       format.json { render json: @chat_room_member.errors, status: :unprocessable_entity }
  #     end
  #   end
  # end

  # # DELETE /chat_room_members/1
  # # DELETE /chat_room_members/1.json
  # private
  #   # Use callbacks to share common setup or constraints between actions.
  #   def set_chat_room_member
  #     @chat_room_member = ChatRoomMember.find(params[:id])
  #   end

    # Never trust parameters from the scary internet, only allow the white list through.
  # def chat_room_member_params
  #   params.require(:chat_room_member).permit(:id, :employee_id, :chat_room_id)
  # end
end
