class MessagesController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource

  def show
    respond_to do |format|
      format.html 
      format.json { render partial: "messages/message", locals: {hide_recipient: false, message: @message} }
    end
  end

  def new
    @message = Message.new
  end

  def edit
  end

  def create
    @message = Message.new(message_params)
    @message.sender_id = @message.anonymous ? nil : current_employee.id
    respond_to do |format|
      if @message.save
        format.html { redirect_to @message, notice: 'Message was successfully created.' }
        format.json { render partial: "messages/message", locals: {hide_recipient: false, message: @message} }
      else
        format.html { render :new }
        format.json { render json: @message.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @message.read_at = Time.now if @message.read_at.nil?
    respond_to do |format|
      if @message.update(update_message_params)
        format.html { redirect_to @message, notice: 'Message was successfully updated.' }
        format.json { render partial: "messages/message", locals: {hide_recipient: false, message: @message} }
      else
        format.html { render :edit }
        format.json { render json: @message.errors, status: :unprocessable_entity }
      end
    end
  end

  # def destroy
  #   @message.destroy
  #   respond_to do |format|
  #     format.html { redirect_to messages_url, notice: 'Message was successfully destroyed.' }
  #     format.json { head :no_content }
  #   end
  # end

  def sent_messages
    @messages = current_employee.sent_messages
  end

  def received_messages
    @received_anonymous_messages = current_employee.received_anonymous_messages
    @received_nonanonymous_messages = current_employee.received_nonanonymous_messages
  end

  private
    # Never trust parameters from the scary internet, only allow the white list through.
    def message_params
      params.require(:message).permit(:body, :anonymous,
        :message_category_id, :sender_id, :recipient_id)
    end

    def update_message_params
      params.require(:message).permit(:status, :read_at)
    end
end
