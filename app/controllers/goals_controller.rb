class GoalsController < ApplicationController
  # before_action :set_goal, only: [:show, :edit, :update, :destroy]
  before_action :set_goal_reference

  before_action :authenticate_user!
  load_and_authorize_resource
  
  # GET /goals
  # GET /goals.json
  def index    
    if @ref_id.blank?
      @goals = Goal.all
    else
      @goals = Goal.where(reference_id: @ref_id, reference_type: @ref_type)
    end  
  end

  # GET /goals/1
  # GET /goals/1.json
  def show    
    @goal = Goal.where(id: params[:id], reference_id: @ref_id, reference_type: @ref_type).first

    respond_to do |format|
      if @goal.blank?
        format.json { render json: 'Record Not found', status: :unprocessable_entity }
      else
        format.json { render :show, status: :created, location: @goal }
      end
    end
  end

  # GET /goals/new
  def new
    @goal = Goal.new
  end

  # GET /goals/1/edit
  def edit
  end

  # POST /goals
  # POST /goals.json
  def create    
    @goal = Goal.new(goal_params.merge(reference_id: @ref_id, reference_type: @ref_type))

    respond_to do |format|
      if @goal.save
        # format.html { redirect_to @goal, notice: 'Goal was successfully created.' }
        format.json { render :show, status: :created, location: @goal }
      else
        # format.html { render :new }
        format.json { render json: @goal.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /goals/1
  # PATCH/PUT /goals/1.json
  def update
    respond_to do |format|
      if @goal.update(goal_params.merge(reference_id: @ref_id, reference_type: @ref_type))
        # format.html { redirect_to @goal, notice: 'Goal was successfully updated.' }
        format.json { render :show, status: :ok, location: @goal }
      else
        # format.html { render :edit }
        format.json { render json: @goal.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /goals/1
  # DELETE /goals/1.json
  def destroy
    @goal.destroy
    respond_to do |format|
      format.html { redirect_to goals_url, notice: 'Goal was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_goal
      @goal = Goal.find(params[:id])
    end

    def set_goal_reference
      ref = request.path.split('/')
      @ref_type = ref[1].singularize.upcase_first
      @ref_id = params[Goal.goals_mapping[ref[1]]]
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def goal_params
      params.require(:goal).permit(:id, :title, :description)
    end
end
