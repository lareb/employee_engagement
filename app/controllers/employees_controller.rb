#TODOs
## soft delete
## All employees API


class EmployeesController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource 

  swagger_controller :employees, "Employees Management"

  swagger_api :index do
    summary "Employees Directory"
    param :query, :alphabet, :string, :optional, "Alphabet ehich need to be sorted"
  end

  swagger_api :create do
    summary "Creates a new Employee"
    param :form, "employee[first_name]", :string, :required
    param :form, "employee[middle_name]", :string, :optional
    param :form, "employee[last_name]", :string, :optional
    param_list :form, "employee[gender]", :string, :optional, "Gender", [ "Male", "Female" ]

    param :form, "employee[date_of_birth]", :string, :required
    param :form, "employee[employee_code]", :string, :required
    param :form, "employee[mobile]", :string, :required
    param_list :form, "employee[job_title_id]", :string, :optional,"Gender", JobTitle.all.map(&:id)
    param_list :form, "freq", :string, :optional, "Frequency", ["daily", "monthly", "monthly"]

    param :form, "employee[about]", :file, :optional

    param :form, "employee[address_line_1]", :string, :required, "Address Line one"
    param :form, "employee[address_line_2]", :string, :optional, "Address Line one"
    param :form, "employee[city]", :string, :required, "City"
    param :form, "employee[state]", :string, :optional, "State"
    param :form, "employee[zip]", :string, :optional, "Zipcode"
    param :form, "employee[country]", :string, :required, "Country"
    param :form, "employee[phone]", :string, :optional, "Phone"   
    param :form, "employee[fax]", :string, :optional, "Fax"   
    param :form, "employee[url]", :string, :optional, "Url"

    response :unauthorized
    response :not_acceptable
  end
  
  swagger_api :update do
    summary "Update Employee"
    param :form, "employee[first_name]", :string, :required
    param :form, "employee[middle_name]", :string, :optional
    param :form, "employee[last_name]", :string, :optional
    param_list :form, "employee[gender]", :string, :required,"Gender", [ "Male", "Female" ]

    param :form, "employee[date_of_birth]", :string, :required
    param :form, "employee[employee_code]", :string, :required
    param :form, "employee[mobile]", :string, :required
    param_list :form, "employee[job_title_id]", :string, :required,"Gender", JobTitle.all.map(&:id)
    param :form, "employee[about]", :file, :optional

    param :form, "employee[address_line_1]", :string, :required, "Address Line one"
    param :form, "employee[address_line_2]", :string, :optional, "Address Line one"
    param :form, "employee[city]", :string, :required, "City"
    param :form, "employee[state]", :string, :optional, "State"
    param :form, "employee[zip]", :string, :optional, "Zipcode"
    param :form, "employee[country]", :string, :required, "Country"
    param :form, "employee[phone]", :string, :optional, "Phone"   
    param :form, "employee[fax]", :string, :optional, "Fax"   
    param :form, "employee[url]", :string, :optional, "Url"
    
    response :unauthorized
    response :not_acceptable
  end

  def index
    @employees = Employee.all
    alphabet = params[:alphabet].try(:downcase) || "a"
    if params[:search]
      @employees = Employee.search(params[:search]).order("employee_id ASC")
    else
      @employees = Employee.where('lower(substr(first_name, 1, 1)) = ?', alphabet).order("employee_code ASC")
    end
    @count = @employees.count
    @total_count = Employee.count

    respond_to do |format|
      format.html # show.html.erb
      format.json #{ render json: @people}
     end    
  end

  def show
  end

  def my_profile
    @employee = current_employee
    respond_to do |format|
      format.html 
      format.json { render :show }
    end    
  end

  # GET /employees/new
  def new
    @employee = Employee.new
    @employee_departments = @employee.employee_departments.build(employee_params)
    @employee_departments = @employee.employee_teams.build(employee_params)
  end

  # GET /employees/1/edit
  def edit
  end

  def create
    @employee = Employee.new(employee_params().merge({created_by: current_user.id, updated_by: current_user.id}))
    ActiveRecord::Base.transaction do
      @employee.save!
      update_employee_skills()
      respond_to do |format|
        format.html { redirect_to @employee, notice: 'Employee was successfully created.' }
        format.json { render :show, status: :ok, location: @employee }
      end
    end
    rescue ActiveRecord::RecordInvalid => e 
    respond_to do |format|
      format.html { render :new }
      format.json { render json: generate_error(:bad_request, e.message), status: :unprocessable_entity }
    end
  end

  def update
    ActiveRecord::Base.transaction do
      @employee.update!(employee_params().merge({created_by: current_user.id, updated_by: current_user.id}))
      update_employee_skills()
      update_employee_interest()
      respond_to do |format|
        format.html { redirect_to @employee, notice: 'Employee was successfully updated.' }
        format.json { render :show, status: :ok, location: @employee }
      end
    end
    rescue ActiveRecord::RecordInvalid => e 
    respond_to do |format|
      format.html { render :edit }
      format.json { render json: generate_error(:bad_request, e.message), status: :unprocessable_entity }
    end
  end

  def update_employee_skills
    primary_skills, secondary_skills = [], []
    #Both will be in form of array of string, i.e [Ruby, Angular JS]

    primary_skills = params[:employee][:primary_skills_params]

    unless primary_skills.blank?
      @employee.primary_skills.destroy_all
      primary_skills = primary_skills.collect{|s| {title: s}}

      primary_skills.each do |p_s|
        p_skill = Skill.find_or_create_by(p_s)
        p_skills = @employee.employee_skills.build({is_primary: true, skill_id: p_skill.try(:id)})
        p_skills.save
      end      
    end

    secondary_skills = params[:employee][:secondary_skills_params]

    unless secondary_skills.blank?
      @employee.secondary_skills.destroy_all
      secondary_skills = secondary_skills.collect{|s| {title: s}}
      
      secondary_skills.each do |p_s|
        p_skill = Skill.find_or_create_by(p_s)
        p_skills = @employee.employee_skills.build({skill_id: p_skill.try(:id)})
        p_skills.save
      end      
    end
  end

  def update_employee_interest
    interests = []
    #Both will be in form of array of string, i.e [Ruby, Angular JS]
    interests = params[:employee][:interests]

    return if interests.blank?

    interests = interests.collect{|s| {title: s}}
    

    @employee.interests.destroy_all
    @employee.reload

    interests.each do |interest|
      t_int = Interest.find_or_create_by(interest)
      t_ints = @employee.employee_interests.build({interest_id: t_int.try(:id)})
      t_ints.save
    end

  end

  def search
    begin
      @employees = Employee.search(params[:search])   
    rescue Exception => e
      render json: generate_error(:invalid_employee_search_request, e.message)
    end
  end

  def destroy
    @employee.destroy
    respond_to do |format|
      format.html { redirect_to employees_url, notice: 'Employee was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_employee
      @employee = Employee.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def employee_params
      if can? :create, Employee
        params.fetch(:employee, {}).permit(:first_name, :middle_name, :last_name, :gender, :date_of_birth, :marital_status, :employee_code, :joining_date, :mobile, :office_phone, :office_phone_ext, :address_line_1, :address_line_2, :city, :state, :zip, :country, :about, :linked_url, :github_url, :facebook_url, :blog_url, :other_references, :job_title_id, :reports_to, :is_active, :user_id, :business_unit_id, :photo, employee_departments_attributes: [:department_id, :is_department_head], employee_teams_attributes: [:team_id], primary_skills_params: [:title, :is_primary], secondary_skills_params: [:title], interests_params: [:title] )
      else
        params.fetch(:employee, {}).permit(:first_name, :middle_name, :last_name, :marital_status, :mobile, :address_line_1, :address_line_2, :city, :state, :zip, :country, :about, :linked_url, :github_url, :facebook_url, :blog_url, :other_references, :photo, primary_skills_params: [:title, :is_primary], secondary_skills_params: [:title], interests_params: [:title] )        
      end
    end
end
