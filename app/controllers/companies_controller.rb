class CompaniesController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource

  swagger_controller :companies, "Company Management"

  swagger_api :create do
    summary "Creates a new Company"
    param :form, "company[name]", :string, :required, "Name"
    param :form, "company[description]", :text, :optional, "Description"
    response :unauthorized
    response :not_acceptable
  end
  
  swagger_api :update do
    summary "Update Company"
    param :form, "company[name]", :string, :required, "Name"
    param :form, "company[description]", :text, :optional, "Description"
    response :unauthorized
    response :not_acceptable
  end

  def index
    # @companies = Company.all
  end

  # GET /companies/1
  # GET /companies/1.json
  def show
  end

  # GET /companies/new
  def new
    @company = Company.first
    if @company.nil?
      @company = Company.new
    else
      redirect_to edit_company_path(@company)
    end
  end

  # GET /companies/1/edit
  def edit
    add_breadcrumb @company.name, company_path(@company)
    add_breadcrumb "Edit #{@company.name}"
  end

  # POST /companies
  # POST /companies.json
  def create
    @company = Company.new(company_params)

    respond_to do |format|
      if @company.save
        format.html { redirect_to new_company_business_unit_path(@company), notice: 'Company was successfully created.' }
        format.json { render :show, status: :created, location: @company }
      else
        format.html { render :new }
        format.json { render json: @company.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /companies/1
  # PATCH/PUT /companies/1.json
  def update
    respond_to do |format|
      if @company.update(company_params)
        format.html { redirect_to company_business_units_path(@company), notice: 'Company was successfully updated.' }
        format.json { render :show, status: :ok, location: @company }
      else
        format.html { render :edit }
        format.json { render json: @company.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /companies/1
  # DELETE /companies/1.json
  def destroy
    @company.destroy
    respond_to do |format|
      format.html { redirect_to companies_url, notice: 'Company was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.

    # Never trust parameters from the scary internet, only allow the white list through.
    def company_params
      params.fetch(:company, {}).permit(:name, :description)
    end
end
