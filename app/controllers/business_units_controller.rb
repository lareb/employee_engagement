class BusinessUnitsController < ApplicationController
  before_action :authenticate_user!

  load_and_authorize_resource :company
  load_and_authorize_resource :business_unit, :through => :company

  before_action :set_breadcrumb

  # swagger_controller :business_unit, "business_unit Management"

  # swagger_api :create do
  #   summary "Creates a new Business Unit"
  #   param :form, "business_unit[title]", :string, :required, "Title"
  #   param :form, "business_unit[is_headquater]", :boolean, :optional, "Is Headquater"
  #   param :form, "business_unit[address_line_1]", :string, :required, "Address Line one"
  #   param :form, "business_unit[address_line_2]", :string, :optional, "Address Line one"
  #   param :form, "business_unit[city]", :string, :required, "City"
  #   param :form, "business_unit[state]", :string, :optional, "State"
  #   param :form, "business_unit[zipcode]", :string, :optional, "Zipcode"
  #   param :form, "business_unit[country]", :string, :required, "Country"
  #   param :form, "business_unit[phone]", :string, :optional, "Phone"   
  #   param :form, "business_unit[fax]", :string, :optional, "Fax"   
  #   param :form, "business_unit[url]", :string, :optional, "Url"

  #   response :unauthorized
  #   response :not_acceptable
  # end
  
  # swagger_api :update do
  #   param :form, "business_unit[title]", :string, :required, "Title"
  #   param :form, "business_unit[is_headquater]", :boolean, :optional, "Is Headquater"
  #   param :form, "business_unit[address_line_1]", :string, :required, "Address Line one"
  #   param :form, "business_unit[address_line_2]", :string, :optional, "Address Line one"
  #   param :form, "business_unit[city]", :string, :required, "City"
  #   param :form, "business_unit[state]", :string, :optional, "State"
  #   param :form, "business_unit[zipcode]", :string, :optional, "Zipcode"
  #   param :form, "business_unit[country]", :string, :required, "Country"
  #   param :form, "business_unit[phone]", :string, :optional, "Phone"   
  #   param :form, "business_unit[fax]", :string, :optional, "Fax"   
  #   param :form, "business_unit[url]", :string, :optional, "Url"

  #   response :unauthorized
  #   response :not_acceptable
  # end

  # GET /business_units
  # GET /business_units.json
  def index
    add_breadcrumb "Business Units", company_business_units_path(@company)
    # @business_units = @company.business_units
  end

  # GET /business_units/1
  # GET /business_units/1.json
  def show
  end

  # GET /business_units/new
  def new
    add_breadcrumb "Business Units", company_business_units_path(@company)
    add_breadcrumb "New Business Units"
  end

  # GET /business_units/1/edit
  def edit
    add_breadcrumb "Business Units", company_business_units_path(@company)
    add_breadcrumb "Edit Business Units"
  end

  # POST /business_units
  # POST /business_units.json
  def create
    @business_unit = @company.business_units.build(business_unit_params)
    # @business_unit = BusinessUnit.new(business_unit_params)

    respond_to do |format|
      if @business_unit.save
        format.html { redirect_to new_company_business_unit_department_path(@company, @business_unit), notice: 'Business unit was successfully created.' }
        format.json { render :show, status: :created, location: business_unit_params }
      else
        format.html { render :new }
        format.json { render json: @business_unit.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /business_units/1
  # PATCH/PUT /business_units/1.json
  def update
    respond_to do |format|
      if @business_unit.update(business_unit_params)
        format.html { redirect_to company_business_unit_departments_path(@company, @business_unit), notice: 'Business unit was successfully updated.' }
        format.json { render :show, status: :ok, location: business_unit_params }
      else
        format.html { render :edit }
        format.json { render json: @business_unit.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /business_units/1
  # DELETE /business_units/1.json
  def destroy
    @business_unit.destroy
    respond_to do |format|
      format.html { redirect_to business_units_url, notice: 'Business unit was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_breadcrumb
      add_breadcrumb @company.name, company_path(@company)
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def business_unit_params
      params.fetch(:business_unit, {}).permit(:title, :is_headquater, :address_line_1, :address_line_2, :city, :state, :zipcode, :country, :phone, :fax, :url, :company_id)
    end
end
