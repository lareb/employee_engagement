class ChatMessagesController < ApplicationController
  before_action :authenticate_user!

  load_and_authorize_resource :chat_room
  load_and_authorize_resource :chat_messages, :through => :chat_room

	def clear_chat_history
		@chat_room.chat_messages.delete_all
   
		respond_to do |format|
			format.js { render action: :show }
			format.json { head :no_content }
		end
	end

end
