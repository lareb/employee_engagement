class HomeController < ApplicationController

	def index
		if current_user.try(:admin?) && Company.first.nil?
			redirect_to new_company_path()
		end

		@company = Company.first
	end
	
end
