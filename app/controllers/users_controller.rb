class UsersController < ApplicationController
	before_action :authenticate_user!

	load_and_authorize_resource :employee
	load_and_authorize_resource :user

  def create
    @user = User.new(user_params.merge({employee_id: @employee.id}))
    respond_to do |format|
      if @user.save
        format.html { redirect_to employee_path(@employee), notice: 'User account has been successfully created.' }
        format.json {render json: {status: :created, message: 'User account has been successfully created.'}}
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @user.update(user_params(:update))
        format.html { redirect_to employee_user_departments_path(@employee, @user), notice: 'User account has been successfully updated.' }
        format.json {render json: { status: :ok, message: 'User account has been successfully updated.' }}
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to employees_url, notice: 'User account has been deleted.' }
      format.json { head :no_content }
    end
  end

	private
	def user_params(action = nil)
    if action == :update
		  params.fetch(:user, {}).permit(:password, :password_confirmation, :employee_id, :role_id)
    else
      params.fetch(:user, {}).permit(:email, :password, :password_confirmation, :employee_id, :role_id)
    end
	end
end
