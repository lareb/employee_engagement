class TeamsController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource :company
  load_and_authorize_resource :business_unit, :through => :company
  load_and_authorize_resource :department, :through => :business_unit
  load_and_authorize_resource :team, :through => :department  

  before_action :set_breadcrumb

  swagger_controller :teams, "Teams Management"

  swagger_api :create do
    summary "Creates a new Team"
    param :form, "team[title]", :string, :required, "Title"
    param :form, "team[description]", :text, :optional, "Description"
    response :unauthorized
    response :not_acceptable
  end

  swagger_api :update do
    summary "Update Team"
    param :form, "team[title]", :string, :required, "Title"
    param :form, "team[description]", :text, :optional, "Description"
    response :unauthorized
    response :not_acceptable
  end

  def index
    add_breadcrumb "Teams"
  end

  def show
  end

  def new
    add_breadcrumb "Teams", company_business_unit_department_teams_path(@company, @business_unit, @department)
    add_breadcrumb "Add New Team"
  end

  def edit
    add_breadcrumb "Teams", company_business_unit_department_teams_path(@company, @business_unit, @department)
    add_breadcrumb "Edit #{@team.title}"
  end

  def create
    @team = @department.teams.build(team_params)
    # @department = @business_unit.departments.build(department_params)
    # @team = Team.new(team_params)
    respond_to do |format|
      if @team.save
        format.html { redirect_to company_business_unit_department_teams_path(@company, @business_unit, @department), notice: 'Team was successfully created.' }
        format.json { render :show, status: :created, location: team_params }
      else
        format.html { render :new }
        format.json { render json: @team.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @team.update(team_params)
        format.html { redirect_to @team, notice: 'Team was successfully updated.' }
        format.json { render :show, status: :ok, location: @team }
      else
        format.html { render :edit }
        format.json { render json: @team.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @team.destroy
    respond_to do |format|
      format.html { redirect_to teams_url, notice: 'Team was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_breadcrumb
      add_breadcrumb @company.name, company_path(@company)
      add_breadcrumb "Business Units", company_business_unit_path(@company, @business_unit)
      add_breadcrumb @department.title, company_business_unit_departments_path(@company, @business_unit, @department)
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def team_params
      params.fetch(:team, {}).permit([:title, :description, :department_id])
    end
end
