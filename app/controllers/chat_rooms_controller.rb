class ChatRoomsController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource :chat_room  

  def index
    @chat_rooms = current_employee.chat_rooms
  end

  def new
    @chat_room = ChatRoom.new
  end

  def create
    if params[:chat_room][:title].blank?
      @chat_room = ChatRoom.new(chat_room_params.merge(employee_id: current_employee.id, title: construct_room_name))
    else
      @chat_room = ChatRoom.new(chat_room_params.merge(employee_id: current_employee.id))
    end

    ActiveRecord::Base.transaction do       
      @chat_room.member_ids = params[:chat_room][:member_ids] unless params[:chat_room][:member_ids].blank?
      @chat_room.save!
      respond_to do |format|
        format.html { redirect_to chat_room_path(@chat_room), success: 'Chat room added!' }
        format.json { render :show, status: :created, location: @chat_room }
      end
    end
    rescue ActiveRecord::RecordInvalid => e 
    respond_to do |format|
      format.html { render :new }
      format.json { render json: generate_error(:bad_request, e.message), status: :unprocessable_entity }
    end
  end

  def show
    @chat_room = ChatRoom.includes(:chat_messages).find_by(id: params[:id])
    @chat_message = ChatMessage.new
  end

  def update
    ActiveRecord::Base.transaction do       
      @chat_room.update_attributes(chat_room_params)
      @chat_room.member_ids = params[:chat_room][:member_ids] unless params[:chat_room][:member_ids].blank?
      @chat_room.save!
      respond_to do |format|
        format.html { redirect_to chat_room_path(@chat_room), success: 'Chat room updated!' }
        format.json { render :show, status: :created, location: @chat_room }
      end
    end
    rescue ActiveRecord::RecordInvalid => e 
    respond_to do |format|
      format.html { render :edit }
      format.json { render json: generate_error(:bad_request, e.message), status: :unprocessable_entity }
    end
  end

  def destroy
    @chat_room.destroy
    respond_to do |format|
      format.html { redirect_to chat_rooms_url, notice: 'Chat room was successfully destroyed.' }
      format.json { render json: {message: 'Chat room was successfully destroyed.'} }
    end
  end  

  def leave
    member = @chat_room.chat_room_members.find_by_employee_id(current_employee.id)
    member.destroy
    respond_to do |format|
      format.html { redirect_to chat_rooms_url, notice: "#{current_employee.first_name} left the chat group #{@chat_room.title}" }
      format.json { render json: {message: "#{current_employee.first_name} left the chat group #{@chat_room.title}"} }
    end    
  end

  private

  def construct_room_name
    target_employees = params[:chat_room][:member_ids]
    target_employees.map!(&:to_i)
    tmp = (target_employees + [current_employee.id]).sort
    tmp.join("<>")
  end

  def chat_room_params
    params.require(:chat_room).permit(:title, :target_employee, member_ids: [:id])
  end
end