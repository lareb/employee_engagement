json.message do 
	json.body chat_message.body
	json.sender chat_message.employee.full_name
	json.sent_at chat_message.created_at
	json.chat_room_id chat_message.chat_room_id
end