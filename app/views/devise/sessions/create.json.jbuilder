json.code 200
json.extract! @user, :id, :email ,:authentication_token, :last_sign_in_at
json.role(@user.role, partial: 'roles/role', as: :role)
json.detail(@user.employee, partial: 'employees/employee', as: :employee)
