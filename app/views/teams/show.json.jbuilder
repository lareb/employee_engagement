json.code 200
json.partial! "teams/team", team: @team
json.members(@team.employees, partial: 'employees/employee', as: :employee)