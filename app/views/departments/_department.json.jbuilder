json.extract! department, :id, :title ,:created_at, :updated_at
json.head(department.head, partial: 'employees/employee', as: :employee)
json.teams(department.teams, partial: 'teams/team', as: :team)