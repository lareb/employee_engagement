json.extract! business_unit, :id, :title, :is_headquater, :address_line_1, :address_line_2, :city, :state, :zipcode, :country, :phone, :fax, :url, :created_at, :updated_at
json.departments(business_unit.departments, partial: 'departments/department', as: :department)
json.dimension business_unit.dimensions

