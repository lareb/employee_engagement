json.extract! employee, :id, :first_name, :middle_name, :last_name, :gender, :date_of_birth, :marital_status, :employee_code, :joining_date, :mobile, :office_phone, :office_phone_ext, :address_line_1, :address_line_2, :city, :state, :zip, :country, :about, :linked_url, :github_url, :facebook_url, :blog_url, :other_references, :created_by, :updated_by, :is_active, :business_unit_id, :created_at, :updated_at

json.job_title employee.job_title.try(:title)
json.login_email employee.user.try(:email)
json.photo employee.photo.url(:medium)
json.url employee_url(employee, format: :json)
json.primary_skills(employee.primary_skills.map(&:title))
json.secondary_skills employee.secondary_skills.map(&:title)
json.interests employee.interests.map(&:title)
json.teams employee.teams

json.departments employee.employee_departments do |department|
	json.id department.department.id
	json.title department.department.title
	json.is_head department.is_department_head
end

json.business_unit employee.business_unit
