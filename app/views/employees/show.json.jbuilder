json.code 200
json.partial! "employees/employee", employee: @employee
json.reports_to_manager @employee.reports_to_manager
json.reportees @employee.reportees
if current_employee.id == @employee.id
	json.goal @employee.goals
end

