json.extract! message_category, :id, :name, :created_at, :updated_at
json.url message_category_url(message_category, format: :json)