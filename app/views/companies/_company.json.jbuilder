json.extract! company, :id, :name, :description, :created_at, :updated_at
json.business_units(company.business_units, partial: 'business_units/business_unit', as: :business_unit)
json.dimension company.dimensions
json.goal company.goals
json.logo company.logo.url
