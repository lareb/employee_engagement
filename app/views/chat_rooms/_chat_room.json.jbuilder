json.extract! chat_room, :id, :title, :created_at, :updated_at
json.created_by(chat_room.created_by, partial: 'employees/employee', as: :employee)
json.members(chat_room.members, partial: 'employees/employee', as: :employee)