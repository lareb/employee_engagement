json.extract! goal, :id, :title, :description, :reference_id, :reference_type, :created_at, :updated_at
json.url goal_url(goal, format: :json)