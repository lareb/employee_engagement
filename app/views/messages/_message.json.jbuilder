json.extract! message, :id, :body, :anonymous, :status, :message_category_id, :sender_id, :recipient_id, :read_at, :created_at, :updated_at

json.message_category message.message_category

if hide_recipient
	json.sender(message.sender, partial: 'employees/employee', as: :employee)
else
	json.recipient(message.recipient, partial: 'employees/employee', as: :employee)
end