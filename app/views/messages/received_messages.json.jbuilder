json.code 200
json.received_nonanonymous_messages @received_nonanonymous_messages, partial: 'messages/message', locals: {hide_recipient: true}, as: :message
json.received_anonymous_messages @received_anonymous_messages, partial: 'messages/message', locals: {hide_recipient: true}, as: :message
