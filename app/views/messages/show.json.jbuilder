json.code 200
json.message(@message, partial: 'messages/message', locals: {hide_recipient: false}, as: :message)